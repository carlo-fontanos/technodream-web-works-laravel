/**
 *  App Class
 *
 *  @created	04/14/2018
 *  @author		Carl Victor C. Fontanos
 */

/**
 * Setup a App namespace
 */
var app = {


	/**
     * Department
     */
    Department: function () {

		/**
		 * This method contains the list of functions that needs to be loaded
		 * when the "Department" object is instantiated.
		 *
		 */
		this.init = function() {
			this.change();
		}


		/**
		 * Change Department
		 */
		this.change = function() {
			if($('input[name=edit-page]').length){
				$('body').on('change', 'select#department_id', function(){
					var self = $(this);
						test_id = $('input[name=edit-page]').val();

					ajax('change-department', home_url + '/admin/tests/update-all', function(response){
						if(response.status == 1){
							highlight_effect(self, '#dff0d8', 500);

						} else if(response.status == 0){
							Lobibox.notify('error', {msg: response.msg, size: 'mini', sound: false});
						}

					}, {'test_id': test_id, 'department_id': self.val()}, 0 );
				});
			}
		}
	},


	/**
     * Test
     */
    Test: function () {

		/**
		 * This method contains the list of functions that needs to be loaded
		 * when the "Test" object is instantiated.
		 *
		 */
		this.init = function() {
			this.create();
			this.remove();
			this.pagination();
		}


		/**
		 * Create New Test
		 */
		this.create = function() {
			$('.create-test').ajaxForm({
				beforeSerialize: function() {
					update_ckeditor_instances();
					wave_box('on');
				},
				success: function(response, textStatus, xhr, form) {
					if(response.status == 0){
						$('.response-container').removeClass('d-n');
						if($('.response-message').html('')){
							if($.isArray(response.msg)){
								$.each(response.msg, function (key, error_message) {
									$('.response-message').append('<li>' + error_message + '</li>');
								});
							}
						}
						scroll_to('.response-container');
					}
					if(response.status == 1){
						window.location = home_url + '/admin/tests/edit/' + response.test_id + '?show-msg=1';
					}
					wave_box('off');
				}
            });

		}

		/**
		 * Create New Test
		 */
		this.remove = function() {
			$('body').on('click', '.remove-test', function(e){
				e.preventDefault();

				if(confirm('Are you sure you want to delete this test?')){
					var _this = $(this);
					var test_id = _this.attr('id');

					ajax('remove-test', home_url + '/admin/tests/destroy', function(response){
						if(response.status == 1){
							_this.parents('tr').remove();
							Lobibox.notify('success', {msg: response.msg, size: 'mini', sound: false});
						} else if(response.status == 0){
							Lobibox.notify('error', {msg: response.msg, size: 'mini', sound: false});
						}
					}, {'test_id': test_id} );
				}
			});
		}

		/**
		 * Load items pagination.
		 */
		this.pagination = function() {

			var _this = this;

			if($('.tests-index-pagination').length){
				if($('form.post-list input').val()){
					data = JSON.parse($('form.post-list input').val());
					_this.pagination_ajax(data.page, data.th_name, data.th_sort);
				} else {
					_this.pagination_ajax(1, 'test_name', 'ASC');
				}

				$('body').on('click', '.post_search_submit', function(){
					var th_active = $('.table-post-list th.active');
					var th_name = $(th_active).attr('id');
					var th_sort = $(th_active).hasClass('DESC') ? 'DESC': 'ASC';
					_this.pagination_ajax(1, th_name, th_sort);
				});

				$(".post_search_text").keyup(function (e) {
					if (e.keyCode == 13) {
						var th_active = $('.table-post-list th.active');
						var th_name = $(th_active).attr('id');
						var th_sort = $(th_active).hasClass('DESC') ? 'DESC': 'ASC';
						_this.pagination_ajax(1, th_name, th_sort);
					}
				});


				$('body').on('click', '.pagination-nav li.active', function(){
					var th_active = $('.table-post-list th.active');
					var th_name = $(th_active).attr('id');
					var page = $(this).attr('p');
					var current_sort = $(th_active).hasClass('DESC') ? 'DESC': 'ASC';
					_this.pagination_ajax(page, th_name, current_sort);
				});


				$('body').on('click', '.table-post-list th', function(e) {
					e.preventDefault();
					var th_name = $(this).attr('id');

					if(th_name){
						if($('.table-post-list th').removeClass('active bg-grey')) {
							$(this).addClass('active bg-grey');
						}
						if(!$(this).hasClass('DESC')){
							_this.pagination_ajax(1, th_name, 'DESC');
							$(this).addClass('DESC');
						} else {
							_this.pagination_ajax(1, th_name, 'ASC');
							$(this).removeClass('DESC');
						}
					}
				});
			}
		}

		/**
		 * AJAX items pagination.
		 */
		this.pagination_ajax = function(page, th_name, th_sort){

			if($(".pagination-container").length){
				wave_box('on');

				var post_data = {
					page: page,
					max: $('.post_max').val(),
					department_id: $('.post_department').val(),
					search: $('.post_search_text').val(),
					th_name: th_name,
					th_sort: th_sort
				};

				$('form.post-list input').val(JSON.stringify(post_data));

				var data = {
					action: "demo_load_my_posts",
					data: JSON.parse($('form.post-list input').val())
				};

				$.ajax({
					url: home_url + '/admin/tests/pagination',
					headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
					type: 'POST',
					data: data,
					datatype: 'JSON',
					success: function (response) {
						if($(".pagination-container").html(response.content)){
							$('.pagination-nav').html(response.navigation);
							wave_box('off');
							scroll_to('.wave-box-wrapper');
							$('.table-post-list th').each(function() {
								/* Append the button indicator */
								$(this).find('i.fa').remove();
								if($(this).hasClass('active')){
									if(JSON.parse($('form.post-list input').val()).th_sort == 'DESC'){
										$(this).append('<i class="fa fa-angle-down pull-right"></i>');
									} else {
										$(this).append('<i class="fa fa-angle-up pull-right"></i>');
									}
								}
							});
						}
					}
				});
			}
		}

	},

	/**
     * Section
     */
    Section: function () {
		/**
		 * This method contains the list of functions that needs to be loaded
		 * when the "Section" object is instantiated.
		 *
		 */
		this.init = function() {
			this.create();
			this.remove();
		}

		/**
		 * Create New Section
		 */
		this.create = function() {
			var self = this;

			$('body').on('click', '.section-create', function(e){
				e.preventDefault();

				var current_list = $('.section-list');
					last_element = current_list.find('.section').last();
					next_number = last_element.attr('number') ? parseInt(last_element.attr('number')) + 1: 1;
					test_id = $('input[name=edit-page]').val();

				if($('input[name=edit-page]').length){
					ajax('create-section', home_url + '/admin/tests/update-all', function(response){
						if(response.status == 1){
							template = self.generate(next_number, response.section_id);
							if(current_list.append(template)){
								var settings = new app.Settings();
								settings.save_input_value_trigger($('input[name=s' + next_number + ']')); /* initialize content saver */

								load_ckeditor('ck-editor-area-s' + next_number + '_content', 200, function(textarea){
									settings.save_ckeditor_content(textarea); /* Initialize saving of ckeditor content to the database */
								});
							}

						} else if(response.status == 0){
							Lobibox.notify('error', {msg: response.msg, size: 'mini', sound: false});
						}

					}, {'test_id': test_id, 'section_order': next_number} );

				} else {
					template = self.generate(next_number);
					if(current_list.append(template)){
						load_ckeditor('ck-editor-area-s' + next_number + '_content', 200);
					}
				}


			});
		}

		/**
		 * Remove Section
		 */
		this.remove = function() {
			$('body').on('click', '.remove-section', function(e){
				e.preventDefault();

				if(confirm('Are you sure you want to remove this section?')){
					if($('input[name=edit-page]').length){
						var section_parent = $(this).parents('.section');

						ajax('remove-section', home_url + '/admin/tests/update-all', function(response){
							if(response.status == 1){
								section_parent.remove();
							} else if(response.status == 0){
								Lobibox.notify('error', {msg: response.msg, size: 'mini', sound: false});
							}
						}, {'section_id': section_parent.attr('section-id')} );

					} else {
						$(this).parents('.section').remove();
					}
				}



			});
		}

		/**
		 * Generate a Section
		 */
		this.generate = function(number, section_id) {
			var number = number ? number: 1;
			var template =
			'<div class="panel panel-default section" number="' + number + '" section-id="' + section_id + '">' +
				'<div class="panel-heading clearfix">' +
					'<a href="" class="fl-r text-danger remove-section"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a>' +
					'<a data-toggle="collapse" href="#collapse-s' + number + '">Section ' + number + '</a>' +
				'</div>' +
				'<div id="collapse-s' + number + '" class="panel-collapse collapse in">' +
					'<div class="panel-body">' +
						'<div class="col-md-6">' +
							'<div class="row">' +
								'<div class="form-group">' +
									'<label for="section-name">Section Name:</label>' +
									'<input type="text" class="form-control" id="section-name" placeholder="Enter section name" name="s' + number + '" e-action="update-section-title" e-id="' + section_id + '" />' +
								'</div>' +
							'</div>' +
						'</div>' +

						'<div class="col-md-12">' +
							'<div class="form-group row">' +
								'<label for="section-desc">Section Description:</label>' +
								'<textarea id="ck-editor-area-s' + number + '_content" e-action="update-section-content" e-id="' + section_id + '" class="form-control editor" name="s' + number + '_content"></textarea>' +
							'</div>' +

							'<br />' +

							'<div class="row question-container">' +
								'<p class="f-b">Questions:<p>' +
								'<div class="col-md-12">	' +
									'<div class="row question-list"></div>' +
								'</div>' +

								'<div class="col-md-6">' +
									'<div class="row">' +
										'<div class="form-group ms-t">' +
											'<select class="form-control w-a d-ib question-type">' +
												'<option value="multiple">Multiple Choices</option>' +
												'<option value="input">Word(s) input</option>' +
												'<option value="essay">Essay - long text</option>' +
											'</select>' +
											'<button class="btn btn-link d-ib question-create"><i class="fa fa-plus"></i> Add Question</button>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>';

			return template;
		}

	},

    /**
     * Question
     */
    Question: function () {

		/**
		 * This method contains the list of functions that needs to be loaded
		 * when the "Question" object is instantiated.
		 *
		 */
		this.init = function() {
			this.create();
			this.remove();
		}

		/**
		 * Create New Question
		 */
		this.create = function() {
			var self = this;

			$('body').on('click', '.question-create', function(e){
				e.preventDefault();

				var question_container = $(this).parents('.question-container');
					question_current_list = question_container.find('.question-list');
					questions = question_current_list.find('.question');

					next_number = questions.length == 0 ? 1: parseInt(questions.last().attr('number')) + 1;
					question_type = question_container.find('.question-type').val();

					section_parent = $(this).parents('.section');
					section_id = section_parent.attr('section-id');
					section_number = 's' + section_parent.attr('number');
					question_number = 'q' + next_number;
					arg = section_number + '_' + question_number;

					if($('input[name=edit-page]').length){
						ajax('create-question', home_url + '/admin/tests/update-all', function(response){
							if(response.status == 1){
								template = self.generate(question_type, arg, next_number, response.question_id, response.choice_ids);
								if(question_current_list.append(template)){

									var settings = new app.Settings();

									settings.save_input_value_trigger($('input[name=' + arg + '_points]')); /* initialize points saver */

									if(question_type == 'input'){
										settings.save_input_value_trigger($('input[name=' + arg + '_answer]')); /* initialize answer saver */
									}

									load_ckeditor('ck-editor-area-' + arg, 200, function(textarea){
										settings.save_ckeditor_content(textarea); /* Initialize saving of ckeditor content to the database */
									});

									var choice = new app.Choice();
									choice.update(); /* Re-initialize typingFinished */

								}

							} else if(response.status == 0){
								Lobibox.notify('error', {msg: response.msg, size: 'mini', sound: false});
							}

						}, {'section_id': section_id, 'question_type': question_type, 'question_order': next_number} );

					} else {
						template = self.generate(question_type, arg, next_number, 0);
						if(question_current_list.append(template)){
							load_ckeditor('ck-editor-area-' + arg, 200);
						}

					}



			});
		}

		/**
		 * Remove Question
		 */
		this.remove = function() {
			$('body').on('click', '.remove-question', function(e){
				e.preventDefault();

				if(confirm('Are you sure you want to remove this question?')){
					if($('input[name=edit-page]').length){
						var question_parent = $(this).parents('.question');

						ajax('remove-question', home_url + '/admin/tests/update-all', function(response){
							if(response.status == 1){
								question_parent.remove();
							} else if(response.status == 0){
								Lobibox.notify('error', {msg: response.msg, size: 'mini', sound: false});
							}
						}, {'question_id': question_parent.attr('question-id')} );

					} else {
						$(this).parents('.question').remove();
					}
				}

			});
		}

		/**
		 * Generate Question base on Type
		 */
		this.generate = function(type, arg, number, question_id, choice_ids) {
			var fields = '';
				choice_a = $.isArray(choice_ids) ? choice_ids[0] : '';
				choice_b = $.isArray(choice_ids) ? choice_ids[1] : '';

			switch(type) {
				case 'multiple':
					fields =
					'<input type="hidden" name="' + arg + '_type" value="multiple" />' +

					'<div class="form-group">' +
						'<label>Question:</label>' +
						'<textarea id="ck-editor-area-' + arg + '" class="form-control editor" name="' + arg + '" e-action="update-question-content" e-id="' + question_id + '"></textarea>' +
					'</div>' +
					'<div class="form-group">' +
						'<label>Choices:</label>' +
						'<div class="pl-l">' +
							'<div class="choices-list">' +
								'<div class="input-group ms-b choice" id="' + choice_a + '">' +
									'<span class="input-group-addon f-b letter" number="1">A</span>' +
									'<input type="text" class="form-control" name="' + arg + '_c1">' +
									'<span class="input-group-addon f-b">' +
										'<input type="checkbox" name="' + arg + '_correct_c1" class="set-answer">' +
									'</span>' +
									'<span class="input-group-addon f-b"> &nbsp; &nbsp; </span>' +
								'</div>' +
								'<div class="input-group ms-b choice" id="' + choice_b + '">' +
									'<span class="input-group-addon f-b letter" number="2">B</span>' +
									'<input type="text" class="form-control" name="' + arg + '_c2">' +
									'<span class="input-group-addon f-b">' +
										'<input type="checkbox" name="' + arg + '_correct_c2" class="set-answer">' +
									'</span>' +
									'<span class="input-group-addon f-b"> &nbsp; &nbsp; </span>' +
								'</div>' +
							'</div>' +

							'<a href="#" class="choices-create"><i class="fa fa-plus"></i> Add New Choice</a>' +
						'</div>' +
					'</div>' +
					'<div class="form-group">' +
						'<label>Points:</label>' +
						'<input type="text" value="" name="' + arg + '_points" class="form-control" e-action="update-question-points" e-id="' + question_id + '" />' +
					'</div>';
					break;

				case 'input':
					fields =
					'<input type="hidden" name="' + arg + '_type" value="input" />' +

					'<div class="form-group">' +
						'<label>Question:</label>' +
						'<textarea id="ck-editor-area-' + arg + '" class="form-control editor" name="' + arg + '" e-action="update-question-content" e-id="' + question_id + '"></textarea>' +
					'</div>' +
					'<div class="form-group">' +
						'<label>Answer:</label>' +
						'<input type="text" value="" name="' + arg + '_answer" class="form-control" e-action="update-question-answer" e-id="' + question_id + '" />' +
					'</div>' +
					'<div class="form-group">' +
						'<label>Points:</label>' +
						'<input type="text" value="" name="' + arg + '_points" class="form-control" e-action="update-question-points" e-id="' + question_id + '" />' +
					'</div>';
					break;

				case 'essay':
					fields =
					'<input type="hidden" name="' + arg + '_type" value="essay" />' +

					'<div class="form-group">' +
						'<label>Question:</label>' +
						'<textarea id="ck-editor-area-' + arg + '" class="form-control editor" name="' + arg + '" e-action="update-question-content" e-id="' + question_id + '"></textarea>' +
					'</div>' +
					'<div class="form-group">' +
						'<label>Points:</label>' +
						'<input type="text" value="" name="' + arg + '_points" class="form-control" e-action="update-question-points" e-id="' + question_id + '" />' +
					'</div>';
					break;
			}

			var template =
			'<div class="panel-group ms-b question" number="' + number + '" id="' + arg + '" question-id="' + question_id + '">' +
				'<div class="panel panel-success">' +
					'<div class="panel-heading clearfix">' +
						'<a href="" class="fl-r text-danger remove-question"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a>' +
						'<a data-toggle="collapse" href="#collapse-' + arg + '">Question ' + number + '</a>' +
					'</div>' +
					'<div id="collapse-' + arg + '" class="panel-collapse collapse">' +
						'<div class="panel-body">' +
							fields +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>';

			return template;
		}

	},

	/**
     * Choice
     */
    Choice: function () {
		/**
		 * This method contains the list of functions that needs to be loaded
		 * when the "Choice" object is instantiated.
		 *
		 */
		this.init = function() {
			this.create();
			this.remove();
			this.update();
		}

		/**
		 * Create New Choice
		 */
		this.create = function() {
			var self = this;

			$('body').on('click', '.choices-create', function(e){
				e.preventDefault();

				var current_list = $(this).siblings('.choices-list');
					last_element = current_list.find('.letter');
					next_number = parseInt(last_element.last().attr('number')) + 1;
					next_letter = get_next_letter(last_element.last().text());
					question_parent = $(this).parents('.question');
					arg = question_parent.attr('id');
					question_id = question_parent.attr('question-id');

				if(question_id > 0){
					ajax('create-choice', home_url + '/admin/tests/update-all', function(response){
						if(response.status == 1){
							template = self.generate(arg, next_letter, next_number, response.choice_id);
							if($(current_list).append(template)){
								self.update(); /* Re-initialize typingFinished */
							}

						} else if(response.status == 0){
							Lobibox.notify('error', {msg: response.msg, size: 'mini', sound: false});
						}
					}, {'question_id': question_id, 'order': next_number} );

				} else {
					template = self.generate(arg, next_letter, next_number, 0);
					$(current_list).append(template);
				}


			});
		}

		/**
		 * Update Choice
		 */
		this.update = function() {
			$('.choice input[type="text"]').each(function(index) {
				var self = $(this);

				self.typingFinished(500, function(element){
					choice_id = self.parents('.choice').attr('id');

					ajax('update-choice', home_url + '/admin/tests/update-all', function(response){
						if(response.status == 1){
							highlight_effect(self, '#dff0d8', 500);
						}
					}, {'choice_id': choice_id, 'choice_content': element.val()}, 0 );
				});
			});
		}


		/**
		 * Remove Choice
		 */
		this.remove = function() {
			$('body').on('click', '.remove-choice', function(e){
				e.preventDefault();

				var self = $(this);
					choice_list = self.parents('.choices-list');
					choice_parent = self.parents('.choice');
					choice_id = choice_parent.attr('id');

				if(confirm('Are you sure you want to remove this choice?')){
					if(choice_id != 0 && self.attr('correct')){
						ajax('remove-choice', home_url + '/admin/tests/update-all', function(response){
							if(response.status == 1){
								/* Remove choice on success */
								if(self.parents('.choice').remove()){
									/* Update choice ordering */
									$(choice_list.find('.letter')).each(function(index) {
										/* Uppercase letters starts with 66 onwards,
										 * 98 onwards for lower case letters
										 */
										$(this).text(String.fromCharCode(65 + index));
									});
								}

							} else if(response.status == 0){
								Lobibox.notify('error', {msg: response.msg, size: 'mini', sound: false});
							}
						}, {id: choice_id, correct: self.attr('correct')});

					} else {
						if(self.parents('.choice').remove()){
							/* Update choice ordering */
							$(choice_list.find('.letter')).each(function(index) {
								/* Uppercase letters starts with 66 onwards,
								 * 98 onwards for lower case letters
								 */
								$(this).text(String.fromCharCode(65 + index));
							});
						}
					}


				}

			});
		}

		/**
		 * Generate a Choice
		 */
		this.generate = function(arg, letter, number, choice_id) {
			var template =
			'<div class="input-group ms-b choice" id="' + choice_id + '">' +
				'<span class="input-group-addon f-b letter" number="' + number + '">' + letter + '</span>' +
				'<input type="text" class="form-control" name="' + arg + '_c' + number + '">' +
				'<span class="input-group-addon f-b">' +
					'<input type="checkbox" name="' + arg + '_correct_c' + number + '" class="set-answer">' +
				'</span>' +
				'<span class="input-group-addon f-b">' +
					'<a href="#" class="remove-choice" correct="0"><i class="fa fa-trash-o text-danger"></i></a>' +
				'</span>' +
			'</div>';

			return template;
		}

	},

	/**
     * Answer
     */
    Answer: function () {

		/**
		 * This method contains the list of functions that needs to be loaded
		 * when the "Answer" object is instantiated.
		 *
		 */
		this.init = function() {
			this.set();
		}


		/**
		 * Set choice as an answer
		 */
		this.set = function() {
			$('body').on('change', '.set-answer', function(e){
				var self = $(this);
					status = this.checked ? 1: 0;
					question_id = self.parents('.question').attr('question-id');
					choice_id = self.parents('.choice').attr('id');

				if(question_id > 0 && choice_id > 0){
					ajax('set-answer', home_url + '/admin/tests/update-all', function(response){
						if(response.status == 1){
							self.parents('.choice').find('.remove-choice').attr('correct', status);

						} else if(response.status == 0){

						}
					}, {'status': status, 'choice_id': choice_id, 'question_id': question_id} );
				}

			});
		}
	},


	/**
     * Applicants
     */
    Applicants: function () {

		/**
		 * This method contains the list of functions that needs to be loaded
		 * when the "Applicants" object is instantiated.
		 *
		 */
		this.init = function() {
			this.pagination();
			this.edit_exam();
			this.remove();

		}

		/**
		 * Load applicants pagination.
		 */
		this.pagination = function() {

			var _this = this;

			if($('.applicants-index-pagination').length){
				if($('form.post-list input').val()){
					data = JSON.parse($('form.post-list input').val());
					_this.pagination_ajax(data.page, data.th_name, data.th_sort);
				} else {
					_this.pagination_ajax(1, 'created_at', 'DESC');
				}

				$('body').on('click', '.post_search_submit', function(){
					var th_active = $('.table-post-list th.active');
					var th_name = $(th_active).attr('id');
					var th_sort = $(th_active).hasClass('DESC') ? 'DESC': 'ASC';
					_this.pagination_ajax(1, th_name, th_sort);
				});

				$(".post_search_text").keyup(function (e) {
					if (e.keyCode == 13) {
						var th_active = $('.table-post-list th.active');
						var th_name = $(th_active).attr('id');
						var th_sort = $(th_active).hasClass('DESC') ? 'DESC': 'ASC';
						_this.pagination_ajax(1, th_name, th_sort);
					}
				});


				$('body').on('click', '.pagination-nav li.active', function(){
					var th_active = $('.table-post-list th.active');
					var th_name = $(th_active).attr('id');
					var page = $(this).attr('p');
					var current_sort = $(th_active).hasClass('DESC') ? 'DESC': 'ASC';
					_this.pagination_ajax(page, th_name, current_sort);
				});


				$('body').on('click', '.table-post-list th', function(e) {
					e.preventDefault();
					var th_name = $(this).attr('id');

					if(th_name){
						if($('.table-post-list th').removeClass('active bg-grey')) {
							$(this).addClass('active bg-grey');
						}
						if(!$(this).hasClass('DESC')){
							_this.pagination_ajax(1, th_name, 'DESC');
							$(this).addClass('DESC');
						} else {
							_this.pagination_ajax(1, th_name, 'ASC');
							$(this).removeClass('DESC');
						}
					}
				});
			}
		}

		/**
		 * AJAX items pagination.
		 */
		this.pagination_ajax = function(page, th_name, th_sort){

			if($(".pagination-container").length){
				wave_box('on');

				var post_data = {
					page: page,
					max: $('.post_max').val(),
					working_time: $('.post_working_time').val(),
					department_id: $('.post_department').val(),
					display: $('.post_display').val(),
					search: $('.post_search_text').val(),
					th_name: th_name,
					th_sort: th_sort
				};

				$('form.post-list input').val(JSON.stringify(post_data));

				var data = {
					action: "demo_load_my_posts",
					data: JSON.parse($('form.post-list input').val())
				};

				$.ajax({
					url: home_url + '/admin/applicants/pagination',
					headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
					type: 'POST',
					data: data,
					datatype: 'JSON',
					success: function (response) {
						if($(".pagination-container").html(response.content)){
							$('.pagination-nav').html(response.navigation);
							wave_box('off');
							scroll_to('.wave-box-wrapper');
							$('.table-post-list th').each(function() {
								/* Append the button indicator */
								$(this).find('i.fa').remove();
								if($(this).hasClass('active')){
									if(JSON.parse($('form.post-list input').val()).th_sort == 'DESC'){
										$(this).append('<i class="fa fa-angle-down pull-right"></i>');
									} else {
										$(this).append('<i class="fa fa-angle-up pull-right"></i>');
									}
								}
							});
						}
					}
				});
			}
		}

		/**
		 * Edit Applicant Examination
		 */
		this.edit_exam = function(){
			if($('.edit-applicant-exam').length){
				$('.applicant-input-score').bind('keyup mouseup', function(){
					var self = $(this);
						question_id = self.attr('id');
						applicant_id = self.attr('applicant');
						test_id = self.attr('test');
						points = self.val();

					if(question_id > 0 && applicant_id > 0){
						ajax('set-answer', home_url + '/admin/applicants/update', function(response){
							if(response.status == 1){
								highlight_effect(self, '#dff0d8', 500);
								$('.total-points').text(response.total_points);
								$('.total-percentage').text(response.total_percentage);
							} else if(response.status == 0){
								console.log(response.msg);
							}
						}, {'points': points, 'applicant_id': applicant_id, 'question_id': question_id, 'test_id': test_id}, 0 );
					}
				});
			}
		}

		/**
		 * Delete Applicant Profile & Examination
		 */
		this.remove = function(){
			var _this = this;

			$('body').on('click', '.delete-applicant', function(e){
				e.preventDefault();
				var applicant_id = $(this).attr('id');
				if(confirm('Are you sure you want to delete this applicant\'s PROFILE and EXAMINATION record? Note: You will not be able to undo this action once started')){
					ajax('delete-applicant', home_url + '/admin/applicants/destroy', function(response){
						if(response.status == 1){
							Lobibox.notify('success', {msg: response.msg, size: 'mini', sound: false});
						} else if(response.status == 0){
							Lobibox.notify('error', {msg: response.msg, size: 'mini', sound: false});
						}
					}, {'applicant_id': applicant_id} );

					var data = JSON.parse($('form.post-list input').val());
					_this.pagination_ajax(data.page, data.th_name, data.th_sort);
				}
			});
		}
	},

	/**
     * Settings
     */
    Settings: function () {

		/**
		 * This method contains the list of functions that needs to be loaded
		 * when the "Settings" object is instantiated.
		 *
		 * This is typically where you define plugins that should be initialized
		 * when the app object gets loaded by the DOM.
		 *
		 */
		this.init = function() {
			this.set_ckeditor();
			this.save_input_value();

		}

		/**
		 * Initialize all instances of CKEditor
		 */
		this.set_ckeditor = function() {
			var self = this;

			if($('textarea.editor').length){
				$('textarea.editor').each(function(index){
					if($('input[name=edit-page]').length){
						load_ckeditor($(this).attr('id'), 200, function(textarea){
							self.save_ckeditor_content(textarea);
						});

					} else {
						load_ckeditor($(this).attr('id'), 200);
					}
				});
			}
		}

		/**
		 * Update database with the new textarea value
		 */
		this.save_ckeditor_content = function(textarea) {
			var action = textarea.attr('e-action');
				id = textarea.attr('e-id');
				content = textarea.val();

			if(id > 0){
				ajax(action, home_url + '/admin/tests/update-all', function(response){
					if(response.status == 1){
						highlight_effect(textarea.parent('.form-group'), '#dff0d8', 500);

					} else if(response.status == 0){

					}
				}, {'id': id, 'content': content}, 0);
			}

		}

		/**
		 * Update database with the new input value
		 */
		this.save_input_value = function(element) {
			var self = this;

			if(element){
				self.save_input_value_trigger(element);

			} else {
				$('input[e-action]').each(function(index) {
					self.save_input_value_trigger($(this));
				});
			}
		}

		this.save_input_value_trigger = function(element) {

			element.typingFinished(1000, function(el){
				var action = element.attr('e-action');
					id = element.attr('e-id');
					content = element.val();

				ajax(action, home_url + '/admin/tests/update-all', function(response){
					if(response.status == 1){
						highlight_effect(element, '#dff0d8', 500);
					} else {
						Lobibox.notify('error', {msg: response.msg, size: 'mini', sound: false});
					}
				}, {'id': id, 'content': content}, 0);
			});

		}
	}
}

/**
 * When the document has been loaded...
 *
 */
jQuery(document).ready( function () {

	department = new app.Department(); // Instantiate the Department Class
	department.init(); // Load Department class methods

	test = new app.Test(); // Instantiate the Test Class
	test.init(); // Load Test class methods

	section = new app.Section(); // Instantiate the Section Class
	section.init(); // Load Section class methods

	question = new app.Question(); // Instantiate the Question Class
	question.init(); // Load Question class methods

	choice = new app.Choice(); // Instantiate the Choice Class
	choice.init(); // Load Choice class methods

	answer = new app.Answer(); // Instantiate the Answer Class
	answer.init(); // Load Answer class methods

	settings = new app.Settings();// Instantiate the Settings Class
	settings.init(); // Load Settings class methods

	applicants = new app.Applicants();// Instantiate the Applicants Class
	applicants.init(); // Load Applicants class methods

});