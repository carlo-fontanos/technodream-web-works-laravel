/**
 * Helper function to easily scroll to the top of the page
 * 
 * @param element
 */
function scroll_to( element ){
	if( element.length ) {
		jQuery('html, body').animate({scrollTop: jQuery(element).offset().top-100}, 150);
	}
}

/**
 * Helper function to get append the loading image to message container when submitting via AJAX
 * 
 * @param textarea, height
 */
function load_ckeditor( textarea, height, callback ) {			
	
	CKEDITOR.config.allowedContent = true;
	CKEDITOR.config.removePlugins = 'about';
	CKEDITOR.plugins.addExternal('codesnippet', home_url + '/public/theme/admin/plugins/ckeditor/plugins/codesnippet/plugin.js');
	CKEDITOR.plugins.addExternal('autogrow', home_url + '/public/theme/admin/plugins/ckeditor/plugins/autogrow/plugin.js');
	
	/* Enable CKFinder plugin */
	CKEDITOR.replace( textarea, {
		toolbar: null,
		toolbarGroups: null,
		extraPlugins: 'uploadimage,codesnippet,autogrow',
		autoGrow_minHeight: 100,
		autoGrow_onStartup: true,
		filebrowserUploadUrl: "http://192.168.2.15/public/upload.php",
		/*
		filebrowserBrowseUrl : home_url + '/public/theme/admin/plugins/ckfinder/ckfinder.html',
		filebrowserImageBrowseUrl : home_url + '/public/theme/admin/plugins/ckfinder/ckfinder.html?type=Images',
		filebrowserFlashBrowseUrl : home_url + '/public/theme/admin/plugins/ckfinder/ckfinder.html?type=Flash',
		filebrowserUploadUrl : home_url + '/public/theme/admin/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl : home_url + '/public/theme/admin/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		filebrowserFlashUploadUrl : home_url + '/public/theme/admin/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
		*/
	});
	
	/* Update  */
	if($.isFunction(callback)){
		var timer = null;
		CKEDITOR.instances[textarea].on('change', function(){
			if(CKEDITOR.instances[textarea].updateElement()){
				clearTimeout(timer);
				timer = setTimeout(callback.bind(textarea, $('#' + textarea)), 1000);
			}
		});
	}
}
		
/**
 * Helper function to command CKEditor to update the instancnes before performing the AJAX call.
 * This will populate the hidden textfields with the proper values coming from the CKEditor 
 *
 */
function update_ckeditor_instances() {
	for ( instance in CKEDITOR.instances ) {
		CKEDITOR.instances[instance].updateElement();
	}
}

/**
 * Provides a highlight effect with options.
 * It also executes even though the browser / tab is not active. 
 * 
 */
function highlight_effect(element, color, speed) {
	jQuery(element).effect("highlight", { color: color }, speed, function () {
		$(this).stop(true, true);
	});
}

/**
 * Provides a nice wave animation effect 
 * 
 */
function wave_box_animate(){
	if( $('.wave-box-effect').length ){
		jQuery( ".wave-box-effect" ).css( "left", "0px" );
		jQuery( ".wave-box-effect" ).animate( { 'left':"99%" }, 1000, wave_box_animate );
	}
}

function wave_box(option) {
	if($('.wave-box-wrapper').length){
		if(option == 'on'){
			if($(".wave-box-wrapper .wave-box").html('<div class="wave-box-effect"></div>').show()){
				wave_box_animate();
			}
		} else if(option == 'off')  {
			$(".wave-box-wrapper .wave-box").html('').fadeOut();
		}
	}
}

/**
 * Generates random code base on the specified parameter
 */
function gen_random(number = 10) {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < number; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

/**
 * Get next letter
 */
function get_next_letter(current_character) {
    return String.fromCharCode(current_character.charCodeAt(0) + 1);
}

/**
 * AJAX Shorthand method
 */
function ajax(action, url, callback, other_data, show_wave_box = 1){
	if(show_wave_box){
		wave_box('on');
	}
	$.ajax({
		type: 'post',
		headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
		url: url,
		data: {'action': action, 'data': other_data},
		success: function(response){
			callback(response);
			if(show_wave_box){
				wave_box('off');
			}
		}
	});
}


/**
 * jQuery typingFinished plugin. Fire a callback function when the user is finished typing.
 *
 * @param timeout  The timeout in milliseconds per interval.
 * @param callback The callback function to fire when the user finishes typing.
 * @return Object
 *
 * Example:
 
	$('input').typingFinished(500, function(element){
		console.log(element.val());
	});
	
 */
$.fn.typingFinished = function(timeout, callback){
	var timer = null,
		self = $(this);
	
	$(self).keydown(function(){
		clearTimeout(timer);
		timer = setTimeout(callback.bind(self, self), timeout);
	});
	
	return self;
}

$(document).ready(function(){
	$('.collapse-all').on('click', function () {
		$('.panel-collapse').collapse('toggle');
	});
});