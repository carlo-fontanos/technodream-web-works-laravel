<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = [
		'applicant_id',
		'department_id',
		'test_id',
		'question_id',
		'choice_id',
		'answer_content',
		'points'
	];
}
