<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    protected $fillable = [
		'applicant_id',
		'working_time',
		'department_id',
		'test_id',
		'first_name',
		'middle_name',
		'last_name',
		'email',
		'phone_number',
		'created_at',
		'updated_at'
	];
}
