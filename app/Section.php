<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = [
		'section_id',
		'test_id',
		'section_name',
		'section_content',
		'section_order',
		'created_at',
		'updated_at'
	];
}
