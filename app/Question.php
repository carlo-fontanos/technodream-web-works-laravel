<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
		'question_id',
		'section_id',
		'question_content',
		'question_points',
		'question_type',
		'question_order',
		'created_at',
		'updated_at'
	];
}
