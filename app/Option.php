<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public $timestamps = false;
	
	public static function add_option( $option_key, $option_value ) {
		
		$option_value = is_array( $option_value ) ? serialize( $option_value ) : $option_value; 
		
		$option_data = array(
			'option_name'		=>	$option_key,
			'option_value'		=>	$option_value
		);
			
		$insert_option = DB::table('options')->insert( $option_data );
		
		if( $insert_option ){
			return true;
		} else {
			return false;
		}
		
	}
	
	public static function update_option( $option_key, $option_value ) {
		
		$option_exits = DB::table('options')
				->where('option_name', $option_key)
				->get();
				
		if( count( $option_exits) > 0 ) {
			
			$option_value = is_array( $option_value ) ? serialize( $option_value ) : $option_value; 
			$option_data = array(
				'option_value'	=>	$option_value
			);
						
			$update_option = DB::table('options')
					->where('option_name', $option_key)
					->update($option_data);
					
			if( $update_option ){
				return true;
			} else {
				return false;
			}
			
		} else {
			return self::add_option( $option_key, $option_value );
			
		}
	}
	
	public static function delete_option( $option_key ) {
		
		if( $option_key ) {		
			DB::table('options')
					->where('option_name', $option_key)
					->delete();
							
			$check_deleted = DB::table('options') 
					->where('option_name', $option_key)
					->value('option_name');
					
			if( count( $check_deleted ) == 0 ){
				return true;
			} else {
				return false;
			}
			
		} else {
			return false;
		}
	}
	
	/**
	 * get_option():
	 *
	 * If the result of the get_option() is serialized, you can use the unserialize() 
	 * function of PHP to convert the data back to array format.
	 * You can use is_serialized() helper function for checking weather the return 
	 * is serailized or not, the return of is_serialized() is boolean.
	*/
	public static function get_option( $option_key ) {
		
		if( $option_key ){
			$option_data = DB::table('options')
					->where('option_name', $option_key)
					->value('option_value');
					
			if( $option_data ){
				return $option_data;
			} else {
				return false;
			}
			
		} else {
			return false;
		}
		
	}
}