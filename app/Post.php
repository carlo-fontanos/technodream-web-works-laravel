<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
		'ID',
		'post_author',
		'post_content',
		'post_title',
		'post_excerpt',
		'post_status',
		'post_password',
		'post_name',
		'post_category',
		'post_parent',
		'guid',
		'post_type'
	];
}
