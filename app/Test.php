<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = [
		'test_id',
		'department_id',
		'test_name',
		'test_content',
		'created_at',
		'updated_at'
	];
}
