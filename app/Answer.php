<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
		'answer_id',
		'answer_content',
		'question_id',
		'created_at',
		'updated_at'
	];
}
