<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$working_time = Auth::user()->role == 'hr-night-time' ? 'night-time' : 'day-time';

		$applicant_count_query = '
			SELECT COUNT(DISTINCT a.applicant_id) AS counts
			FROM applicants a
			LEFT JOIN results r ON a.applicant_id = r.applicant_id
			WHERE r.applicant_id > 0 AND a.working_time = "'.$working_time.'"';

		return view('admin.home', [
			'applicants_day' 	=> DB::select($applicant_count_query . '
				AND YEAR(a.created_at) = YEAR(NOW())
				AND MONTH(a.created_at) = MONTH(NOW())
				AND DAY(a.created_at) = DAY(NOW());', []
			)[0]->counts,

			'applicants_week'	=> DB::select($applicant_count_query . '
				AND WEEKOFYEAR(a.created_at) = WEEKOFYEAR(NOW());', []
			)[0]->counts,

			'applicants_month'	=> DB::select($applicant_count_query . '
				AND YEAR(a.created_at) = YEAR(NOW())
				AND MONTH(a.created_at) = MONTH(NOW());', []
			)[0]->counts,

			'applicants_count'	=> DB::select($applicant_count_query, [] )[0]->counts,

			'applicants'	=> DB::select('
				SELECT DISTINCT a.applicant_id AS id, a.* FROM applicants a
				LEFT JOIN results r ON a.applicant_id = r.applicant_id
				WHERE r.applicant_id > 0 AND a.working_time = "'.$working_time.'" ORDER BY a.created_at DESC LIMIT 5', []
			),

			'tests'				=> DB::table('tests AS t')->leftJoin('departments AS d', 'd.department_id', '=', 't.department_id')->select('t.*', 'd.department_name')->orderBy('t.created_at', 'DESC')->take(10)->get(),
			'departments'		=> DB::table('departments')->orderBy('department_name')->get()
		]);
    }
}
