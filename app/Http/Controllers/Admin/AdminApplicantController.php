<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use Validator;

use App\Applicant;
use App\Test;
use App\Section;
use App\Question;
use App\Choice;
use App\Answer;
use App\Result;
use App\Department;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminApplicantController extends Controller
{

    public function index()
    {
        return view('admin.applicants.index');
    }

	public function index_pagination(Request $request) {

		$pag_content = '';
		$pag_navigation = '';

		if( $request->ajax() ) {

			if( isset( $request['data']['page'] ) ){

				$page = $request['data']['page']; /* Current Page */
				$name = $request['data']['th_name']; /* Name of the column to sort */
				$sort = $request['data']['th_sort']; /* Order (DESC or ASC) */
				$cur_page = $page;
				$page -= 1;
				$per_page = $request['data']['max']; /* Number of items to display per page */
				$previous_btn = true;
				$next_btn = true;
				$first_btn = true;
				$last_btn = true;
				$start = $page * $per_page;

				$where_search = '';

				if( Auth::user() ){
					$working_time = Auth::user()->role == 'hr-night-time' ? 'night-time' : 'day-time';
					$where_search .= ' AND a.working_time = "' . $working_time . '" ';
				}

				/* Check if there is a string inputted on the search box */
				if( ! empty( $request['data']['search']) ){
					/* If a string is inputted, include an additional query logic to our main query to filter the results */
					$where_search .= ' AND (
						a.first_name LIKE "%' . $request['data']['search'] . '%" OR
						a.middle_name LIKE "%' . $request['data']['search'] . '%" OR
						a.last_name LIKE "%' . $request['data']['search'] . '%" OR
						a.email LIKE "%' . $request['data']['search'] . '%"
					)';
				}

				if( ! empty( $request['data']['department_id'] ) ){
					$where_search .= ' AND a.department_id = ' . $request['data']['department_id'] . ' ';
				}

				if( ! empty( $request['data']['display'] ) ){
					switch( $request['data']['display'] ){
						case 'today':
							$where_search .= ' AND YEAR(a.created_at) = YEAR(NOW()) AND MONTH(a.created_at) = MONTH(NOW()) AND DAY(a.created_at) = DAY(NOW()) ';
							break;
						case 'week':
							$where_search .= ' AND WEEKOFYEAR(a.created_at) = WEEKOFYEAR(NOW()) ';
							break;
						case 'month':
							$where_search .= ' AND YEAR(a.created_at) = YEAR(NOW()) AND MONTH(a.created_at) = MONTH(NOW()) ';
							break;
					}
				}

				/* Retrieve all the posts */
				$all_test = DB::select('
					SELECT
						r.applicant_id,
						a.created_at,
						a.first_name,
						a.middle_name,
						a.last_name,
						a.email,
						a.phone_number,
						t.test_name,
						d.department_name,
						a.working_time,
						SUM(r.points) AS applicant_test_points,
						SUM(q.question_points) AS total_test_points,
						ROUND((SUM(r.points) / SUM(q.question_points)) * 100, 2) AS applicant_test_percentage
					FROM results r
						LEFT JOIN questions q ON q.question_id = r.question_id
						LEFT JOIN applicants a ON a.applicant_id = r.applicant_id
						LEFT JOIN tests t ON t.test_id = r.test_id
						LEFT JOIN departments d ON d.department_id = r.department_id
					WHERE a.applicant_id > 0' .  $where_search . '
					GROUP BY r.applicant_id, t.test_id, d.department_id, a.created_at, a.first_name, a.middle_name, a.last_name, a.email, a.phone_number, t.test_name, d.department_name, a.working_time
					ORDER BY ' . $name . ' ' .  $sort . '
					LIMIT ?, ?', [$start, $per_page] );

				$count = DB::select('
					SELECT COUNT(DISTINCT a.applicant_id) AS count
					FROM applicants a
					LEFT JOIN results r ON a.applicant_id = r.applicant_id
					WHERE r.applicant_id > 0' . $where_search, [] );

				/* Check if our query returns anything. */
				if( $all_test ){

					/* Iterate thru each test */
					foreach( $all_test as $key => $test ){
						$pag_content .= '
						<tr>
							<td>
								<a href="' . url('/admin/applicants/show/' . $test->applicant_id ) . '" class="f-u m-r">' . $test->last_name . '</a>
							</td>
							<td>' . $test->first_name . '</td>
							<td>' . $test->middle_name . '</td>
							<td>' . $test->email . '</td>
							<td>' . $test->phone_number . '</td>
							<td>' . $test->applicant_test_points . ' / ' . $test->total_test_points . '</td>
							<td>' . $test->applicant_test_percentage . '%</td>
							<td>' . $test->test_name . '</td>
							<td>' . ucfirst(str_replace('_', '-', $test->working_time) ) . '</td>
							<td>' . $test->department_name . '</td>
							<td>' . date('F j, Y, g:i A', strtotime( $test->created_at ) ) . '</td>
							<td>
								<a href="' . url('/admin/applicants/show/' . $test->applicant_id ) . '" class="f-u m-r">Profile</a>
								<a href="' . url('/admin/applicants/edit/' . $test->applicant_id ) . '" class="f-u m-r">Exam</a>
								<a href="#" id="' . $test->applicant_id . '" class="f-u delete-applicant">Delete</a>
							</td>
						</tr>';
					}

				/* If the query returns nothing, we throw an error message */
				} else {
					$pag_content .= '<td colspan = "7" class = "bg-danger p-d">No results found.</td>';

				}

				$pag_content = $pag_content . "<br class = 'clear' />";

				$no_of_paginations = ceil($count[0]->count / $per_page);

				if ($cur_page >= 7) {
					$start_loop = $cur_page - 3;
					if ($no_of_paginations > $cur_page + 3)
						$end_loop = $cur_page + 3;
					else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
						$start_loop = $no_of_paginations - 6;
						$end_loop = $no_of_paginations;
					} else {
						$end_loop = $no_of_paginations;
					}
				} else {
					$start_loop = 1;
					if ($no_of_paginations > 7)
						$end_loop = 7;
					else
						$end_loop = $no_of_paginations;
				}

				$pag_navigation .= "<ul>";

				if ($first_btn && $cur_page > 1) {
					$pag_navigation .= "<li p='1' class='active'>First</li>";
				} else if ($first_btn) {
					$pag_navigation .= "<li p='1' class='inactive'>First</li>";
				}

				if ($previous_btn && $cur_page > 1) {
					$pre = $cur_page - 1;
					$pag_navigation .= "<li p='$pre' class='active'>Previous</li>";
				} else if ($previous_btn) {
					$pag_navigation .= "<li class='inactive'>Previous</li>";
				}
				for ($i = $start_loop; $i <= $end_loop; $i++) {

					if ($cur_page == $i)
						$pag_navigation .= "<li p='$i' class = 'selected' >{$i}</li>";
					else
						$pag_navigation .= "<li p='$i' class='active'>{$i}</li>";
				}

				if ($next_btn && $cur_page < $no_of_paginations) {
					$nex = $cur_page + 1;
					$pag_navigation .= "<li p='$nex' class='active'>Next</li>";
				} else if ($next_btn) {
					$pag_navigation .= "<li class='inactive'>Next</li>";
				}

				if ($last_btn && $cur_page < $no_of_paginations) {
					$pag_navigation .= "<li p='$no_of_paginations' class='active'>Last</li>";
				} else if ($last_btn) {
					$pag_navigation .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
				}

				$pag_navigation = $pag_navigation . "</ul>";
			}


			$response = array(
				'content' 		=>	$pag_content,
				'navigation' 	=>	$pag_navigation,
			);

			return response()->json( compress_output( $response ) );
		}
	}

	public function show($id)
    {
		$applicant = Applicant::where('applicant_id', $id)->first();

		if( ! $applicant ){
			abort(404);
		}

		$department = Department::where('department_id', $applicant->department_id)->first();

		return view('admin.applicants.show', [
			'applicant'		=> $applicant,
			'department' 	=> $department,
		]);
	}


	public function store(Request $request)
	{
		header('Access-Control-Allow-Origin: *');

		$post = $request->all();

		$err_succ = array(
			'id' 		=> '',
			'status' 	=> 0,
			'msg' 		=> ''
		);

		if(
			! cv($post, 'first_name') ||
			! cv($post, 'middle_name') ||
			! cv($post, 'last_name') ||
			! cv($post, 'email') ||
			! cv($post, 'phone') ||
			! cv($post, 'department_id') ||
			! cv($post, 'test_id') ||
			! cv($post, 'working_time')
		){
			$err_succ['msg'] = 'Please fill up all required fields.';

		/* Validate email */
		} else if( ! filter_var( $post['email'], FILTER_VALIDATE_EMAIL ) ){
			$err_succ['msg'] = 'Invalid email address';

		/* Validate phone */
		} else if( preg_match("/[a-z]/i", $post['phone'] ) ){
			$err_succ['msg'] = 'Phone number should not contain letters';

		} else {
			/* Create Applicant */
			$create_applicant = Applicant::create([
				'working_time' 		=> $post['working_time'],
				'department_id' 	=> $post['department_id'],
				'test_id' 			=> $post['test_id'],
				'first_name' 		=> $post['first_name'],
				'middle_name' 		=> $post['middle_name'],
				'last_name' 		=> $post['last_name'],
				'email' 			=> $post['email'],
				'phone_number' 		=> $post['phone']

			]);

			if( $create_applicant->id ){
				$err_succ['user_id'] = $create_applicant->id;
				$err_succ['msg'] = 'Applicant record successfully created.';
				$err_succ['status'] = 1;
			} else {
				$err_succ['msg'] = 'An error occured, please reload this page then try again.';
			}
		}


		return response()->json( $err_succ );
	}

	public function edit($id)
    {
		$result = DB::table('questions AS q')
			->leftJoin('results AS r', 'r.question_id', '=', 'q.question_id')
			->leftJoin('sections AS s', 'q.section_id', '=', 's.section_id')
			->leftJoin('answers AS a', 'a.question_id', '=', 'q.question_id')
			->select(
				'q.*',
				'a.answer_content AS correct',
				'r.applicant_id',
				'r.test_id',
				'r.choice_id AS applicant_choice',
				'r.answer_content AS applicant_input',
				's.section_name',
				'r.points'
			)
			->where('r.applicant_id', $id)
			->orderBy('s.section_order', 'ASC')
			->orderBy('q.question_order', 'ASC')
			->get();

		if( ! $result ){
			abort(404);
		}

		return view('admin.applicants.edit', [
			'result'	=> $result,
		]);
    }

	public function update(Request $request)
    {
		if( $request->ajax() ) {

			$post = $request->all();

			$err_succ = array(
				'id' 		=> '',
				'status' 	=> 0,
				'msg' 		=> ''
			);

			if(
				! cv($post['data'], 'question_id') ||
				! cv($post['data'], 'applicant_id') ||
				! isset( $post['data']['points'] )

			){
				$err_succ['msg'] = 'Missing variable(s).';

			} else {
				Result::
					where([
						['question_id',  $post['data']['question_id']],
						['applicant_id',  $post['data']['applicant_id']]
					])
					->update(
						array('points' => $post['data']['points'])
					);

				$applicant_points = Result::
					where([
						['test_id',  $post['data']['test_id']],
						['applicant_id',  $post['data']['applicant_id']]
					])
					->sum('points');

				$test_max_points = DB::table('questions AS q')
					->leftJoin('sections AS s', 'q.section_id', '=', 's.section_id')
					->leftJoin('tests AS t', 's.test_id', '=', 't.test_id')
					->where('t.test_id',  $post['data']['test_id'])
					->sum('q.question_points');

				$err_succ['total_points'] = $applicant_points;
				$err_succ['total_percentage'] = number_format(($applicant_points / $test_max_points) * 100, 2);
				$err_succ['msg'] = 'Points successfully saved.';
				$err_succ['status'] = 1;
			}

			return response()->json( $err_succ );
		}
	}

	public function destroy(Request $request)
    {
        header('Access-Control-Allow-Origin: *');

		$post = $request->all();

		$err_succ = array(
			'id' 		=> '',
			'status' 	=> 0,
			'msg' 		=> ''
		);

		if( ! cv($post['data'], 'applicant_id') ){
			$err_succ['msg'] = 'Error: missing applicant ID.';

		} else {
			Result::where('applicant_id', $post['data']['applicant_id'])->delete();
			Applicant::where('applicant_id', $post['data']['applicant_id'])->delete();

			$err_succ['msg'] = 'Applicant record successfully deleted.';
			$err_succ['status'] = 1;
		}

		return response()->json( $err_succ );
    }
}