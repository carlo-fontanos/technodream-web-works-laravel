<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use Validator;

use App\Department;
use App\Test;
use App\Section;
use App\Question;
use App\Choice;
use App\Answer;
use App\Result;
use App\Applicant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminTestController extends Controller
{
    
    public function index()
    {	
        return view('admin.tests.index');
    }
	
	public function index_pagination(Request $request) {
		
		$pag_content = '';
		$pag_navigation = '';
		
		if( $request->ajax() ) {
			
			if( isset( $request['data']['page'] ) ){
				
				$page = $request['data']['page']; /* Current Page */
				$name = $request['data']['th_name']; /* Name of the column to sort */
				$sort = $request['data']['th_sort']; /* Order (DESC or ASC) */
				$cur_page = $page;
				$page -= 1;
				$per_page = $request['data']['max']; /* Number of items to display per page */
				$previous_btn = true;
				$next_btn = true;
				$first_btn = true;
				$last_btn = true;
				$start = $page * $per_page;
				
				$where_search = '';
				
				/* Check if there is a string inputted on the search box */
				if( ! empty( $request['data']['search']) ){
					/* If a string is inputted, include an additional query logic to our main query to filter the results */
					$where_search .= ' AND (test_name LIKE "%' . $request['data']['search'] . '%") ';
				}
				
				if( ! empty( $request['data']['department_id'] ) ){
					$where_search .= ' AND t.department_id = ' . $request['data']['department_id'] . ' ';
				}
				
				/* Retrieve all the posts */
				$all_test = DB::select('
					SELECT t.test_id, t.test_name, d.department_name, t.created_at 
					FROM tests t
					LEFT JOIN departments d ON t.department_id = d.department_id
					WHERE t.test_id > 0' .  $where_search . ' 
					ORDER BY ' . $name . ' ' .  $sort . ' LIMIT ?, ?', [$start, $per_page] );
				
				$count = DB::select('
					SELECT COUNT(test_id) as count FROM tests AS t WHERE t.test_id > 0' . $where_search, [] );
				
				/* Check if our query returns anything. */
				if( $all_test ){
					
					/* Iterate thru each test */
					foreach( $all_test as $key => $test ){
						$pag_content .= '
						<tr>
							<td>
								<a href = "' . url('/admin/tests/edit/' . $test->test_id ) . '">
									' . $test->test_name . '
								</a>
							</td>
							<td>' . $test->department_name . '</td>
							<td>' . date('F j, Y, g:i a', strtotime( $test->created_at ) ) . '</td>
							<td>
								<a href="' . url('/admin/tests/edit/' . $test->test_id ) . '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
								<a href="#" class="remove-test m-l" id="' . $test->test_id . '"><i class="fa fa-trash-o"></i> Delete</a>
							</td>
						</tr>';         
					}
					
				/* If the query returns nothing, we throw an error message */
				} else {
					$pag_content .= '<td colspan = "7" class = "bg-danger p-d">No results found.</td>';
					
				}

				$pag_content = $pag_content . "<br class = 'clear' />";
				
				$no_of_paginations = ceil($count[0]->count / $per_page);

				if ($cur_page >= 7) {
					$start_loop = $cur_page - 3;
					if ($no_of_paginations > $cur_page + 3)
						$end_loop = $cur_page + 3;
					else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
						$start_loop = $no_of_paginations - 6;
						$end_loop = $no_of_paginations;
					} else {
						$end_loop = $no_of_paginations;
					}
				} else {
					$start_loop = 1;
					if ($no_of_paginations > 7)
						$end_loop = 7;
					else
						$end_loop = $no_of_paginations;
				}
				  
				$pag_navigation .= "<ul>";

				if ($first_btn && $cur_page > 1) {
					$pag_navigation .= "<li p='1' class='active'>First</li>";
				} else if ($first_btn) {
					$pag_navigation .= "<li p='1' class='inactive'>First</li>";
				} 

				if ($previous_btn && $cur_page > 1) {
					$pre = $cur_page - 1;
					$pag_navigation .= "<li p='$pre' class='active'>Previous</li>";
				} else if ($previous_btn) {
					$pag_navigation .= "<li class='inactive'>Previous</li>";
				}
				for ($i = $start_loop; $i <= $end_loop; $i++) {

					if ($cur_page == $i)
						$pag_navigation .= "<li p='$i' class = 'selected' >{$i}</li>";
					else
						$pag_navigation .= "<li p='$i' class='active'>{$i}</li>";
				}
				
				if ($next_btn && $cur_page < $no_of_paginations) {
					$nex = $cur_page + 1;
					$pag_navigation .= "<li p='$nex' class='active'>Next</li>";
				} else if ($next_btn) {
					$pag_navigation .= "<li class='inactive'>Next</li>";
				}

				if ($last_btn && $cur_page < $no_of_paginations) {
					$pag_navigation .= "<li p='$no_of_paginations' class='active'>Last</li>";
				} else if ($last_btn) {
					$pag_navigation .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
				}

				$pag_navigation = $pag_navigation . "</ul>";	
			}
		
		
			$response = array(
				'content' 		=>	$pag_content,
				'navigation' 	=>	$pag_navigation,
			);
			
			return response()->json( compress_output( $response ) );
		}
	}
    
	public function create()
    {
        return view('admin.tests.create');
    }
	
	public function store_all(Request $request)
	{
		if( $request->ajax() ) {
			
			$post = $request->all();
			
			$err_succ = array(
				'status' 	=> 0,
				'msg'	 	=> array(),
				'ready'	 	=> 1,
				'test_id'	=> 0
			);
			
			/* Step 1: Formatting 
			 * 		   Format the posted data into a nested array for easier processing 
			 */
			$head = [];
			foreach( $post as $key => $input ){
				if( $key[0] == 's' ){
					if( strpos( $key, "_" ) !== false ){
						$factor = explode("_", $key);
						if( $factor[1] == "content" ){
							$head['sections'][$factor[0]]['content'] = $input;
						} else {
							if( count( $factor ) > 2 ){
								if( $factor[2] == "type" || $factor[2] == "points" || $factor[2] == "answer" ){
									$head['sections'][$factor[0]]['questions'][$factor[1]][$factor[2]] = $input;
								} else if( $factor[2] == 'correct' ){
									$head['sections'][$factor[0]]['questions'][$factor[1]]['answers'][$factor[3]] = $input;
								} else {
									$head['sections'][$factor[0]]['questions'][$factor[1]]['choices'][$factor[2]] = $input;
								}
							} else {
								$head['sections'][$factor[0]]['questions'][$factor[1]]['title'] = $input;
							}
						}
					} else {
						$head['sections'][$key]['title'] = $input;
					}
				} else {
					$head[$key] = $input;
				}
			}
			
			
			
			/**
			 * Step 2: Validations 
			 */
			
			/* Check if department_id is included in the post request */
			if( ! isset( $post['department_id'] ) ){
                $err_succ['msg'][] = 'Missing department variable';
                $err_succ['ready'] = 0;
            }
            
            /* Check if test name was provided */
            if( empty( $post['test_name'] ) ){
                $err_succ['msg'][] = 'Test name cannot be empty';
                $err_succ['ready'] = 0;
            }
            
			/* Sections */
            if( isset( $head['sections']  ) ){
                foreach( $head['sections'] as $key_section => $section ){
					$section_position = substr($key_section, 1);
					
					/* Check if all sections have names */ 
					if( ! $section['title'] ){
                        $err_succ['msg'][] = 'Section ' . $section_position . ' name cannot be empty';
						$err_succ['ready'] = 0;
					}
					
					/* Questions */
					if( isset( $section['questions'] ) ){
						foreach( $section['questions'] as $key_question => $question ){
							$question_position = substr($key_question, 1);
							
							/* Check if question title is empty */
							if( ! $question['title'] ){
								$err_succ['msg'][] = 'Title under Section ' . $section_position . ' Question ' . $question_position . ' cannot be empty';
								$err_succ['ready'] = 0;
							}
							
							/* Check if question points is empty */
							if( ! $question['points'] ){
								$err_succ['msg'][] = 'Points under Section ' . $section_position . ' Question ' . $question_position . ' cannot be empty';
								$err_succ['ready'] = 0;
							}
							
							/* Check if question type is empty */
							if( ! $question['type'] ){
								$err_succ['msg'][] = 'Type under Section ' . $section_position . ' Question ' . $question_position . ' is missing';
								$err_succ['ready'] = 0;
							}
							
							/* Choices and Correct Answers */
							if( $question['type'] == 'multiple' ){
								if( isset( $question['choices'] ) ){
									foreach( $question['choices'] as $key_choice => $choice ){
										$choice_position = substr($key_choice, 1);
										
										/* Check if the choice is empty */
										if( $choice == "" ){
											$err_succ['msg'][] = 'Choice ' . $choice_position .  ' under Section ' . $section_position . ' Question ' . $question_position . ' cannot be empty';
											$err_succ['ready'] = 0;
										}
									}
								}
								
								/* Check if there is/are any selected correct answer(s) for the given question */
								if( ! isset( $question['answers'] ) || empty( $question['answers'] ) ){
									$err_succ['msg'][] = 'Please select a correct answer for Section ' . $section_position . ' Question ' . $question_position;
									$err_succ['ready'] = 0;
								}
							
							} else if( $question['type'] == 'input' ){
								/* Check if there is a text answer provided for the given question */
								if( ! isset( $question['answer'] ) || empty( $question['answer'] ) ){
									$err_succ['msg'][] = 'Please provide a correct answer for Section ' . $section_position . ' Question ' . $question_position;
									$err_succ['ready'] = 0;
								}
							}
							
						}
					}
					
                    
                }
            }
			
			/**
			 * Step 3: Data Processing 
			 */
						
			if( $err_succ['ready'] ){
				if( isset( $post['test_name'] ) && isset( $post['department_id'] ) ){
					/* Create Test */
					$create_test = Test::create([
						'department_id' 	=> $post['department_id'],
						'test_name' 		=> $post['test_name'],
						'test_content' 		=> $post['test_content']
					]);
					
					$err_succ['test_id'] = $create_test->id;
				
					if( isset( $head['sections'] ) ){
						foreach( $head['sections'] as $key_section => $section ){
							/* Create Section */
							$create_section = Section::create([
								'test_id' 			=> $create_test->id,
								'section_name' 		=> $section['title'],
								'section_content' 	=> $section['content'],
								'section_order' 	=> substr($key_section, 1),
							]);
							
							if( isset( $section['questions'] ) ){
								foreach( $section['questions'] as $key_question => $question ){
									/* Create Question */
									$create_question = Question::create([
										'section_id' 		=> $create_section->id,
										'question_content' 	=> $question['title'],
										'question_points' 	=> $question['points'],
										'question_type' 	=> $question['type'],
										'question_order' 	=> substr($key_question, 1),
									]);
									
									
									if( $question['type'] == 'multiple' ){
										$choice_list = array();
										
										if( isset( $question['choices'] ) ){
											foreach( $question['choices'] as $key_choice => $choice ){
												/* Create Choice */
												$create_choice = Choice::create([
													'question_id' 		=> $create_question->id,
													'choice_content' 	=> $choice,
													'choice_order' 		=> substr($key_choice, 1),
												]);
												
												$choice_list[substr($key_choice, 1)] = $create_choice->id;
											}
										}
										
										if( isset( $question['answers'] ) ){
											foreach( $question['answers'] as $key_answer => $answer ){
												/* Create Multiple Answers */
												$create_answer = Answer::create([
													'answer_content' 	=> $choice_list ? $choice_list[substr($key_answer, 1)] : '', /* Get choice ID from the list of choices */
													'question_id' 		=> $create_question->id,
												]);
											}
										}
									
									} else if( $question['type'] == 'input' ){
										if( isset( $question['answer'] ) ){
											/* Create Single Text Answer */
											$create_answer = Answer::create([
												'question_id' 		=> $create_question->id,
												'answer_content' 	=> $question['answer'],
											]);
										}
									}
									
								
								}
							}
							
							
						}
						
						
					}
				}
				
				$err_succ['status'] = 1;
				$err_succ['msg'] = 'Test successfully created';
			}
			
			return response()->json( $err_succ );
		}
	}
	
	public function store(Request $request)
	{
		if( $request->ajax() ){
			
			$err_succ = array(
				'id' 		=> '',
				'status' 	=> 0,
				'msg' 		=> ''
			);
			
			$validation = Validator::make( $request->all(), [
				'department_id' => 'required|exists:departments,department_id',
				'test_name' 	=> 'required'
			]);
			
			if( $validation->passes() ){
				$create_test = Test::create([
					'department_id' 	=> $request->department_id,
					'test_name' 		=> $request->test_name
				]);
				
				$err_succ['id'] = $create_test->id;
				$err_succ['status'] = 1;
				$err_succ['msg'] = 'Test successfully created';
			
			}
			
			if( $validation->fails() ){
				$err_succ['msg'] = $validation->errors()->all();
			}
			
			
			return response()->json( $err_succ );
		}
	}
	
    public function edit($id)
    {
		$test = Test::where('test_id', $id)->first();
		
		if( ! $test){
			abort(404);
		}
		
		return view('admin.tests.edit', [
			'test'			=> $test,
			'departments'	=> Department::all(),
			'tree'			=> get_test_tree( $id, 'object' ),
		]);
    }
	
	public function update(Request $request)
    {	
	
	}
	
	public function update_all(Request $request)
    {	
        if( $request->ajax() ){
			
			$post = $request->all();
			
			$err_succ = array(
				'status' 	=> 0,
				'msg' 		=> ''
			);
			
			/* Choices */
				/* Update Choice */
				if( $post['action'] == 'update-choice' ){
					
					$update_choice = Choice
						::where('choice_id', $post['data']['choice_id'])
						->update(
							array( 'choice_content' =>  $post['data']['choice_content'] )
						);
					
					$err_succ['status'] = 1;
					$err_succ['msg'] = 'Choice successfully updated';
				}
				
				/* Create Choice */
				if( $post['action'] == 'create-choice' ){
					
					$create_choice = Choice::create([
						'question_id' 		=> $post['data']['question_id'],
						'choice_content' 	=> '',
						'choice_order' 		=> $post['data']['order']
					]);
					
					$err_succ['choice_id'] = $create_choice->id;
					$err_succ['status'] = 1;
					$err_succ['msg'] = 'Choice successfully created';
				}
				
				/* Delete Choice */
				if( $post['action'] == 'remove-choice' ){
					
					Result::where('choice_id', $post['data']['id'])->delete();
					Choice::where('choice_id', $post['data']['id'])->delete();
					
					/* Delete from `answers` table if currently assigned as correct answer to a question */
					if( $post['data']['correct'] ){
						Answer::where('answer_content', $post['data']['id'])->delete();
					}
					
					$err_succ['status'] = 1;
					$err_succ['msg'] = 'Choice successfully deleted';
				}
				
				/* Set Answer */
				if( $post['action'] == 'set-answer' ){
					if( $post['data']['status'] == 1 ){
						/* Create a new answer */
						Answer::create([
							'question_id' 		=> $post['data']['question_id'],
							'answer_content' 	=> $post['data']['choice_id']
						]);
						
						$err_succ['status'] = 1;
						$err_succ['msg'] = 'Choice successfully set as a correct answer';
						
					} else if( $post['data']['status'] == 0 ){
						/* Delete answer */
						Answer::where('answer_content', $post['data']['choice_id'])->delete();
						
						$err_succ['status'] = 1;
						$err_succ['msg'] = 'Choice successfully unset as correct answer';
						
					}
				}
				
			/* Questions */
				/* Update Question */
					/* Content */
					if( $post['action'] == 'update-question-content' ){
						Question::where('question_id', $post['data']['id'])
								->update(array( 'question_content' =>  $post['data']['content'] ));
						$err_succ['status'] = 1;
						$err_succ['msg'] = 'Question content successfully updated';
					}
					/* Points */
					if( $post['action'] == 'update-question-points' ){
						if( ! empty( $post['data']['content'] ) && ! is_numeric( $post['data']['content'] ) ){
							$err_succ['msg'] = 'Question points must be an number without decimals or special characters';
							
						} else {
							Question::where('question_id', $post['data']['id'])
									->update(array( 'question_points' =>  $post['data']['content'] ));
							$err_succ['status'] = 1;
							$err_succ['msg'] = 'Question points successfully updated';
						}
					}
					/* Answer */
					if( $post['action'] == 'update-question-answer' ){
						Answer::where('question_id', $post['data']['id'])
								->update(array( 'answer_content' =>  $post['data']['content'] ));
						$err_succ['status'] = 1;
						$err_succ['msg'] = 'Answer successfully updated';
					}
				
				/* Create Question */
				if( $post['action'] == 'create-question' ){
					
					$create_question = Question::create([
						'section_id' 		=> $post['data']['section_id'],
						'question_type' 	=> $post['data']['question_type'],
						'question_order' 	=> $post['data']['question_order']
					]);
					
					/* Create 2 pre-made choices if type of question is multiple choice */
					$choice_ids = array();
					if( $post['data']['question_type'] == 'multiple' ){
						for( $i = 1; $i <= 2; $i++ ){
							$create_choice = Choice::create([
								'question_id' 		=> $create_question->id,
								'choice_content' 	=> '',
								'choice_order' 		=> $i
							]);
							$choice_ids[] = $create_choice->id;
						}
					}
					
					if( $post['data']['question_type'] == 'input' ){
						Answer::create([
							'question_id' => $create_question->id,
						]);
					}
												
					$err_succ['question_id'] = $create_question->id;
					$err_succ['choice_ids'] = $choice_ids;
					$err_succ['status'] = 1;
					$err_succ['msg'] = 'Question successfully created';
				}
				
				/* Delete Question */
				if( $post['action'] == 'remove-question' ){
					
					Result::where('question_id', $post['data']['question_id'])->delete();
					Answer::where('question_id', $post['data']['question_id'])->delete();
					Choice::where('question_id', $post['data']['question_id'])->delete();
					Question::where('question_id', $post['data']['question_id'])->delete();
					
					$err_succ['status'] = 1;
					$err_succ['msg'] = 'Choice successfully deleted';
				}
			
			/* Sections */
				/* Update Section */
					/* Title */
					if( $post['action'] == 'update-section-title' ){
						Section::where('section_id', $post['data']['id'])
								->update(array( 'section_name' =>  $post['data']['content'] ));
						$err_succ['status'] = 1;
						$err_succ['msg'] = 'Section title successfully updated';
					}
					/* Content */
					if( $post['action'] == 'update-section-content' ){
						Section::where('section_id', $post['data']['id'])
								->update(array( 'section_content' =>  $post['data']['content'] ));
						$err_succ['status'] = 1;
						$err_succ['msg'] = 'Section content successfully updated';
					}
				
				/* Create Section */
				if( $post['action'] == 'create-section' ){
					
					$create_section = Section::create([
						'test_id' 			=> $post['data']['test_id'],
						'section_order' 	=> $post['data']['section_order']
					]);
											
					$err_succ['section_id'] = $create_section->id;
					$err_succ['status'] = 1;
					$err_succ['msg'] = 'Section successfully created';
				}
				
				/* Delete Section */
				if( $post['action'] == 'remove-section' ){
					
					/* Select all questions under the section */
					$questions = Question::where('section_id', $post['data']['section_id'])->get();
					if( $questions ){
						foreach( $questions as $key => $question ){
							/* Delete all Applicant Answers */
							Result::where('question_id', $question->question_id)->delete();
							
							/* Delete all choices */
							Answer::where('question_id', $question->question_id)->delete();
							
							/* Delete all answer */
							Choice::where('question_id', $question->question_id)->delete();
						}
					}
					
					Question::where('section_id', $post['data']['section_id'])->delete();
					Section::where('section_id', $post['data']['section_id'])->delete();
					
					$err_succ['status'] = 1;
					$err_succ['msg'] = 'Choice successfully deleted';
				}
				
			/* Test */
				/* Update Test */
					/* Title */
					if( $post['action'] == 'update-test-title' ){
						Test::where('test_id', $post['data']['id'])
								->update(array( 'test_name' =>  $post['data']['content'] ));
						$err_succ['status'] = 1;
						$err_succ['msg'] = 'Test title successfully updated';
					}
					/* Content */
					if( $post['action'] == 'update-test-content' ){
						Test::where('test_id', $post['data']['id'])
								->update(array( 'test_content' =>  $post['data']['content'] ));
						$err_succ['status'] = 1;
						$err_succ['msg'] = 'Test description successfully updated';
					}
					
			/* Department */
				/* Update Department */
					/* Change */
					if( $post['action'] == 'change-department' ){
						Test::where('test_id', $post['data']['test_id'])
								->update(array( 'department_id' =>  $post['data']['department_id'] ));
						$err_succ['status'] = 1;
						$err_succ['msg'] = 'Department successfully changed';
					}
					
					/* Set as active test for department */
					if( $post['action'] == 'set-department_test' ){
						Department::where('department_id', $post['data']['department_id'])
								->update(array( 'test_id' =>  $post['data']['test_id'] ));
						$err_succ['status'] = 1;
						$err_succ['msg'] = 'Test successfully set as default for selected department';
					}
				
				
			return response()->json( $err_succ );
		}
    }
    
    public function show()
    {
        return view('admin.tests.show');
    }
    
    
	public function destroy(Request $request)
    {
        if( $request->ajax() ){
			
			$post = $request->all();
			
			$err_succ = array(
				'status' 	=> 0,
				'msg' 		=> ''
			);
			
			if( $post['action'] == 'remove-test' ){
				/* Select all questions under the section */
				$sections = Section::where('test_id', $post['data']['test_id'])->get();
				if( $sections ){
					foreach( $sections as $key => $section ){
						/* Select all questions under the section */
						$questions = Question::where('section_id', $section->section_id)->get();
						foreach( $questions as $key => $question ){
							/* Delete all choices */
							Answer::where('question_id', $question->question_id)->delete();
							
							/* Delete all answer */
							Choice::where('question_id', $question->question_id)->delete();
						}
						
						Question::where('section_id', $section->section_id)->delete();
						Section::where('section_id', $section->section_id)->delete();
					}
				}
				
				/* Delete all applicant answers connected to this test */
				Result::where('test_id', $post['data']['test_id'])->delete();
				
				/* Delete all applicants connected to this test */
				Applicant::where('test_id', $post['data']['test_id'])->delete();
				
				/* Delete test */
				Test::where('test_id', $post['data']['test_id'])->delete();
				
				$err_succ['status'] = 1;
				$err_succ['msg'] = 'Test successfully deleted';
			}
			
			return response()->json( $err_succ );
		}
    }
}