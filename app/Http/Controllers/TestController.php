<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;


class TestController extends Controller
{

    public function get_question(Request $request)
    {
        header('Access-Control-Allow-Origin: *');
		
		$post = $request->all();
		$test_id = cv($post, 'test_id');
		$user_id = cv($post, 'user_id', 0);
		$page = cv($post, 'page', 1);
		$per_page = cv($post, 'limit', 1);
		
		/* Let's make sure the applicant't record is available */
		get_set_applicant($post);
		
		if( ! $user_id  || ! $test_id ){
			$response['error'] = 'Invalid applicant, no user ID';
			
		} else {
			$question = \DB::table('questions AS q')
				->leftJoin('sections AS s', 's.section_id', '=', 'q.section_id')
				->leftJoin('tests AS t', 't.test_id', '=', 's.test_id')
				->select('q.*', 's.section_id', 's.section_name', 's.section_content', 's.section_order')
				->where('t.test_id', $test_id)
				->orderBy('s.section_order', 'ASC')
				->orderBy('q.question_order', 'ASC')
				->offset(($page -1) * $per_page)
                ->limit(1)
				->get();
				
			$count = \DB::table('questions AS q')
				->leftJoin('sections AS s', 's.section_id', '=', 'q.section_id')
				->leftJoin('tests AS t', 't.test_id', '=', 's.test_id')
				->where('t.test_id', $test_id)
				->count();

			$response['sections'] = get_sections( $test_id );
			$response['question'] = $question[0];
			$response['choices'] = $question[0]->question_type == 'multiple' ? get_choices( $question[0]->question_id ): '';
			$response['applicant_answer'] = get_applicant_answer( $user_id, $question[0]->question_id );
			$response['total'] = ceil( $count / $per_page ); /* Total Pages */
			$response['total_items'] = $count;
		}
		
		return response()->json( $response );
	}
	
	public function get_test(Request $request)
    {
        header('Access-Control-Allow-Origin: *');
		
		$err_succ = array(
			'status' 	=> 0,
			'msg'	 	=> 'An error occurred, please reload the page to try again.'
		);
			
		$post = $request->all();
		$test_id = cv($post, 'test_id');
		$test = \DB::table('tests')->where('test_id', $test_id)->first();
		
		if( $test ){
			$err_succ['status'] = 1;
			$err_succ['msg'] = $test;
		}
		
		return response()->json( $err_succ );
	}
	
	
	public function get_tests(Request $request)
    {
        header('Access-Control-Allow-Origin: *');
		
		$post = $request->all();
		
		$err_succ = array(
			'tests' 	=> '',
			'status' 	=> 0,
			'msg' 		=> ''
		);
		
		if( ! cv($post, 'department_id') ){
			$err_succ['msg'] = 'Missing department';
			
		} else {
			/** 
			 * This will make the test ID: 30 show in the result as requested by Sam Borcena
			 * to avoid data entrying the same set of questions for Sales and Human Resource departments.
			 */
			$include_test = array();
			if( in_array( $post['department_id'], array(3, 5) ) ){
				$include_test[] = \DB::table('tests')->where('test_id', 30)->first();
				$err_succ['msg'] = 'Rendered from custom query';
			}
			
			$tests = get_tests( $post['department_id'] );
			$tests_object = $tests ? $tests : array();
			
			$err_succ['status'] = 1;
			$err_succ['tests'] = array_merge( $tests_object->toArray(), $include_test );
		}
		
		return response()->json( $err_succ );
    }
		
		
	public function set_answer(Request $request)
    {
        header('Access-Control-Allow-Origin: *');
		
		$post = $request->all();
		
		$err_succ = array(
			'status' 	=> 0,
			'msg' 		=> ''
		);
		
		if( 
			! cv($post, 'department_id') || 
			! ( isset($_POST['answer_content']) || cv($post, 'choice_id') ) ||
			! cv($post, 'user_id') || 
			! cv($post, 'question_id') || 
			! cv($post, 'test_id') 
		){
			$err_succ['msg'] = 'Missing variable(s)';
			
		} else {
			
			$question = \App\Question::where('question_id', $post['question_id'])->first();
			$correct_answer = \App\Answer::where('question_id', $post['question_id'])->first();
			$points = 0;
			
			if( $question->question_type == 'multiple' ){
				if( $correct_answer->answer_content == $post['choice_id'] ){
					$points = $question->question_points;
				}
			} else if( $question->question_type == 'input' ){
				if( $correct_answer->answer_content == $post['answer_content'] ){
					$points = $question->question_points;
				}
			}
			
			$result = \App\Result::updateOrCreate(['applicant_id' => $post['user_id'], 'question_id' => $post['question_id']], [ 
				'department_id' => $post['department_id'],
				'choice_id' => cv($post, 'choice_id', null),
				'answer_content' => isset($_POST['answer_content']) ? $_POST['answer_content'] : null,
				'question_id' => $post['question_id'],
				'applicant_id' => $post['user_id'],
				'test_id' => $post['test_id'],
				'points' => $points
			]);

			$err_succ['status'] = 1;
			$err_succ['msg'] = 'Answer successfully saved';
		}
		
		return response()->json( $err_succ );
    }

}