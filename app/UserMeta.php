<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
	public $timestamps = false;
	
	protected $table = 'user_meta';
	
    public static function add_user_meta( $user_id, $meta_key, $meta_value ) {
		
		if( $user_id ) {
			if( $meta_value ) {
				$meta_value = is_array( $meta_value ) ? serialize( $meta_value ) : $meta_value; 
				
				$user_meta_data = array(
					'user_id'		=>	$user_id,
					'meta_key'		=>	$meta_key,
					'meta_value'	=>	$meta_value
				);
					
				$insert_user_meta = DB::table('user_meta')->insert( $user_meta_data );
				
				if( $insert_user_meta ){
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}
	
	public static function update_user_meta( $user_id, $meta_key, $meta_value ) {
			
		if( $user_id ) {
			$user_meta_exits = DB::table('user_meta')
				->where('user_id', $user_id) 
				->where('meta_key', $meta_key)
				->get();
				
			if( count( $user_meta_exits ) > 0 ) {
				
				$meta_value = is_array( $meta_value ) ? serialize( $meta_value ) : $meta_value; 
				$user_meta_data = array(
					'meta_value'	=>	$meta_value
				);
							
				$update_user_meta = DB::table('user_meta')
					->where('user_id', $user_id)
					->where('meta_key', $meta_key)
					->update($user_meta_data);
					
				if( $update_user_meta ){
					return true;
				} else {
					return false;
				}
				
			} else {
				return self::add_user_meta( $user_id, $meta_key, $meta_value );
				
			}
		}
	}
	
	public static function delete_user_meta( $user_id, $meta_key ) {
				
		if( $user_id ) {
			if( $meta_key ) {
				if( is_array( $meta_key ) ){
					foreach( $meta_key as $key ){
						DB::table('user_meta')
							->where('meta_key', $key) 
							->where('user_id', $user_id) 
							->delete();
					}
					
					return true;
					
				} else {
					DB::table('user_meta')
						->where('meta_key', $meta_key)
						->where('user_id', $user_id)
						->delete();
						
					$check_deleted = DB::table('user_meta') 
						->where('meta_key', $meta_key)
						->value('meta_key');
					
					if( count( $check_deleted ) == 0 ){
						return true;
					} else {
						return false;
					}
				}
				
			} else {
				return false;
			}
		}
	}
	
	/**
	 * get_user_meta():
	 *
	 * If the result of the get_user_meta() is serialized, you can use the unserialize() 
	 * function of PHP to convert the data back to array format.
	 * You can use is_serialized() helper function for checking weather the return 
	 * is serailized or not, the return of is_serialized() is boolean.
	 *
	 * @param user_id		ID of the item referenced from the "item" table
	 * @param meta_key		If left empty, it returns all meta values in array format
	 *
	 */
	public static function get_user_meta( $user_id, $meta_key = '' ) {
		
		if( $user_id ) {
			if( $meta_key ){
				$user_meta_data = DB::table('user_meta')
					->where('meta_key', $meta_key)
					->where('user_id', $user_id)
					->value('meta_value');
					
				if( $user_meta_data ){
					return $user_meta_data;
				} else {
					return false;
				}
				
			} else {
				$user_meta_data = DB::table('user_meta')
					->select('meta_key', 'meta_value')
					->where('user_id', $user_id)
					->get();
					
				$array_meta_data = array();
				
				if( $user_meta_data ){
					foreach( $user_meta_data as $key => $meta ){
						$array_meta_data[$meta->meta_key] = $meta->meta_value;
					}
					return (object) $array_meta_data;
				} else {
					return false;
				}
			}
		}
	}