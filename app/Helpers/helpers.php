<?php

if( ! function_exists('pr') ) {

	function pr( $a ) {
		print( '<pre>' ); print_r( $a ); print( '</pre>' );
	}
}

if( ! function_exists('autop') ) {
	
	function autop($pee) {
		$pre_tags = array();

		if ( trim($pee) === '' )
			return '';

		$pee = $pee . "\n"; // just to make things a little easier, pad the end

		if ( strpos($pee, '<pre') !== false ) {
			$pee_parts = explode( '</pre>', $pee );
			$last_pee = array_pop($pee_parts);
			$pee = '';
			$i = 0;

			foreach ( $pee_parts as $pee_part ) {
				$start = strpos($pee_part, '<pre');

				// Malformed html?
				if ( $start === false ) {
					$pee .= $pee_part;
					continue;
				}

				$name = "<pre wp-pre-tag-$i></pre>";
				$pre_tags[$name] = substr( $pee_part, $start ) . '</pre>';

				$pee .= substr( $pee_part, 0, $start ) . $name;
				$i++;
			}

			$pee .= $last_pee;
		}

		$pee = preg_replace('|<br />\s*<br />|', "\n\n", $pee);
		// Space things out a little
		$allblocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|form|map|area|blockquote|address|math|style|p|h[1-6]|hr|fieldset|legend|section|article|aside|hgroup|header|footer|nav|figure|details|menu|summary)';
		$pee = preg_replace('!(<' . $allblocks . '[^>]*>)!', "\n$1", $pee);
		$pee = preg_replace('!(</' . $allblocks . '>)!', "$1\n\n", $pee);
		$pee = str_replace(array("\r\n", "\r"), "\n", $pee); // cross-platform newlines

		if ( strpos( $pee, '<option' ) !== false ) {
			// no P/BR around option
			$pee = preg_replace( '|\s*<option|', '<option', $pee );
			$pee = preg_replace( '|</option>\s*|', '</option>', $pee );
		}

		if ( strpos( $pee, '</object>' ) !== false ) {
			// no P/BR around param and embed
			$pee = preg_replace( '|(<object[^>]*>)\s*|', '$1', $pee );
			$pee = preg_replace( '|\s*</object>|', '</object>', $pee );
			$pee = preg_replace( '%\s*(</?(?:param|embed)[^>]*>)\s*%', '$1', $pee );
		}

		if ( strpos( $pee, '<source' ) !== false || strpos( $pee, '<track' ) !== false ) {
			// no P/BR around source and track
			$pee = preg_replace( '%([<\[](?:audio|video)[^>\]]*[>\]])\s*%', '$1', $pee );
			$pee = preg_replace( '%\s*([<\[]/(?:audio|video)[>\]])%', '$1', $pee );
			$pee = preg_replace( '%\s*(<(?:source|track)[^>]*>)\s*%', '$1', $pee );
		}

		$pee = preg_replace("/\n\n+/", "\n\n", $pee); // take care of duplicates
		// make paragraphs, including one at the end
		$pees = preg_split('/\n\s*\n/', $pee, -1, PREG_SPLIT_NO_EMPTY);
		$pee = '';

		foreach ( $pees as $tinkle ) {
			$pee .= '<p>' . trim($tinkle, "\n") . "</p>\n";
		}

		$pee = preg_replace('|<p>\s*</p>|', '', $pee); // under certain strange conditions it could create a P of entirely whitespace
		$pee = preg_replace('!<p>([^<]+)</(div|address|form)>!', "<p>$1</p></$2>", $pee);
		$pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee); // don't pee all over a tag
		$pee = preg_replace("|<p>(<li.+?)</p>|", "$1", $pee); // problem with nested lists
		$pee = preg_replace('|<p><blockquote([^>]*)>|i', "<blockquote$1><p>", $pee);
		$pee = str_replace('</blockquote></p>', '</p></blockquote>', $pee);
		$pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)!', "$1", $pee);
		$pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee);

		
		$pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*<br />!', "$1", $pee);
		$pee = preg_replace('!<br />(\s*</?(?:p|li|div|dl|dd|dt|th|pre|td|ul|ol)[^>]*>)!', '$1', $pee);
		$pee = preg_replace( "|\n</p>$|", '</p>', $pee );

		if ( !empty($pre_tags) )
			$pee = str_replace(array_keys($pre_tags), array_values($pre_tags), $pee);

		return $pee;
	}
}

if( ! function_exists('limit_words') ) {
	
	function limit_words($text, $limit) {
		
		$words = explode(" ",$text);
		
		return implode(" ",array_splice($words,0,$limit));
		
	}
	
}

if( ! function_exists('get_local_time') ) {
	
	function get_local_time($format) {
		
		date_default_timezone_set('Asia/Manila');
		$curtime = new DateTime();
		
		return $curtime->format($format);
		
	}
	
}

if( ! function_exists('generate_random_code') ) {
	
	function generate_random_code($length=10) {
 
		$string = '';
		$characters = "23456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";

		for ($p = 0; $p < $length; $p++) {
		   $string .= $characters[mt_rand(0, strlen($characters)-1)];
		}

		return $string;
	}
	
}

if( ! function_exists('set_active') ) {
	function set_active( $route ) {
		
		if( is_array( $route ) ){
			return in_array(Request::path(), $route) ? 'active' : '';
		}
		
		return Request::path() == $route ? 'active' : '';
		
	}
}

/**
 * compress_output();
 *
 * This function seem reliable in terms of full code compression, 
 * but the problem is that it also removes spaces between commas.
 * Only use this when compressing DOM elements, like header or footer output.
 *
 */
if( ! function_exists('compress_output') ) {
	function compress_output( $buffer ) {

		# remove comments, tabs, spaces, newlines, etc.
		$search = array(
			"/\/\*(.*?)\*\/|[\t\r\n]/s" => "",
			"/ +\{ +|\{ +| +\{/" => "{",
			"/ +\} +|\} +| +\}/" => "}",
			"/ +: +|: +| +:/" => ":",
			"/ +; +|; +| +;/" => ";",
			"/ +, +|, +| +,/" => ","
		);
		$buffer = preg_replace(array_keys($search), array_values($search), $buffer);
		
		return $buffer;
		
	}
}

/**
 * compress_output_light();
 *
 * this is a light version of the sanitize_output above, 
 * I got the function from this link: http://php.net/manual/en/function.ob-start.php#71953
 * Problem with this function is sometimes it can not compress javascript code properly which 
 * is causing the page to return multiple lines of reponse code one line.
 *
 */
 
if( ! function_exists('compress_output_light') ) {
	function compress_output_light( $buffer ) {
	   
		$search = array(
			'/\>[^\S ]+/s', # strip whitespaces after tags, except space
			'/[^\S ]+\</s', # strip whitespaces before tags, except space
			'/(\s)+/s'  	# shorten multiple whitespace sequences
			);
		$replace = array(
			'>',
			'<',
			'\\1'
			);
		$buffer = preg_replace($search, $replace, $buffer);

		return $buffer;

	}
}

if( ! function_exists('is_mobile') ) {
	function is_mobile() {
		
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
			return true;
		} else {
			return false;
		}
		
	}
}


if( ! function_exists('data_encode') ) {
	function data_encode( $string_or_int ){
		$key = '#&$f7@fss#vgg3f7';	
		$encoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string_or_int, MCRYPT_MODE_CBC, md5(md5($key))));
		
		return $encoded;
	}
}

if( ! function_exists('data_decode') ) {
	function data_decode( $string_or_int ){
		$key = '#&$f7@fss#vgg3f7';	
		$decoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string_or_int), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		
		return $decoded;
	}
}

if( ! function_exists('get_departments') ) {
	function get_departments(){
		return \App\Department::get();
	}
}

if( ! function_exists('get_tests') ) {
	function get_tests( $department_id ){
		return \App\Test::where('department_id', $department_id)->get();
	}
}

if( ! function_exists('get_sections') ) {
	function get_sections( $test_id ){
		return \App\Section::where('test_id', $test_id)
			->orderBy('section_order', 'ASC')
			->get();
	}
}

if( ! function_exists('get_questions') ) {
	function get_questions( $section_id ){
		return \App\Question::where('section_id', $section_id)
			->orderBy('question_order', 'ASC')
			->get();
	}
}

if( ! function_exists('get_choices') ) {
	function get_choices( $question_id ){
		return \App\Choice::where('question_id', $question_id)
			->orderBy('choice_order', 'ASC')
			->get();
	}
}

if( ! function_exists('get_answers') ) {
	function get_answers( $question_id ){
		return \App\Answer::where('question_id', $question_id)->get();
	}
}

if( ! function_exists('get_applicant_answer') ) {
	function get_applicant_answer( $user_id, $question_id ){
		return \App\Result::where([['question_id', $question_id], ['applicant_id', $user_id]])->select('choice_id', 'answer_content')->first();
	}
}

if( ! function_exists('get_set_applicant') ) {
	function get_set_applicant( $post ){
		$applicant = \App\Applicant::where('applicant_id', $post['user_id'])->first();
		
		if( ! $applicant ){
			/* Create Applicant */
			$create_applicant = \App\Applicant::create([
				'applicant_id' 		=> cv($post, 'user_id'),
				'department_id' 	=> cv($post, 'department_id'),
				'test_id' 			=> cv($post, 'test_id'),
				'first_name' 		=> cv($post, 'first_name'),
				'middle_name' 		=> cv($post, 'middle_name'),
				'last_name' 		=> cv($post, 'last_name'),
				'email' 			=> cv($post, 'email'),
				'phone_number' 		=> cv($post, 'phone')
				
			]);
			
			return $create_applicant->id;
		}
		
		return 0;
	}
}

if( ! function_exists('get_test_tree') ) {
	function get_test_tree( $test_id, $type = 'array' ){
		
		$tree = array();
		
		$sections = get_sections( $test_id );
		if( $sections ){
			foreach( $sections as $key => $section ){
				
				$tree['sections'][$section->section_order] = $section->getOriginal();
				$questions = get_questions( $section->section_id );
				if( $questions ){
					foreach( $questions as $key => $question ){
						
						$tree['sections'][$section->section_order]['questions'][$question->question_order] = $question->getOriginal();
						$choices = get_choices( $question->question_id );
						if( $choices ){
							foreach( $choices as $key => $choice ){
								$tree['sections'][$section->section_order]['questions'][$question->question_order]['choices'][$choice->choice_order] = $choice->getOriginal();
							}
						}
						
						$answers = get_answers( $question->question_id );
						if( $answers ){
							foreach( $answers as $key => $answer ){
								$tree['sections'][$section->section_order]['questions'][$question->question_order]['answers'][] = $answer->answer_content;
							}
						}
						
					}
				}
			}
		}
		
		if( $type == 'json' ){
			$data = json_encode( $tree );
			
		} else if( $type == 'object' ){
			$data = json_decode( json_encode( $tree ) );
			
		} else if( $type == 'array' ){
			$data = $tree;
		}
		
		return $data;
	}
}

function get_test_questions( $test_id ){
	$questions = \DB::select('
		SELECT q.*, s.section_name, s.section_content, s.section_order FROM questions q 
		LEFT JOIN sections s ON s.section_id = q.section_id 
		LEFT JOIN tests t ON t.test_id = s.test_id
		WHERE t.test_id = ?
		ORDER BY s.section_order, q.question_order', [$test_id]
	);
	
	return (object) $questions;
}

function cr( $name, $default = '' ){
	if( isset( $_REQUEST[$name] ) && ! empty( $_REQUEST[$name] ) ){
		return $_REQUEST[$name];
	} else {
		return $default;
	}
}

function cv( $array, $name, $default = '' ){
	if( isset( $array[$name] ) && ! empty( $array[$name] ) ){
		return $array[$name];
	} else {
		return $default;
	}
}