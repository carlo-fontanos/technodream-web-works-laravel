<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    protected $fillable = [
		'choice_id',
		'question_id',
		'choice_content',
		'choice_correct',
		'choice_order',
		'created_at',
		'updated_at'
	];
}
