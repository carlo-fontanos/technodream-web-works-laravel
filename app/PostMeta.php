<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{
	public $timestamps = false;
	
	protected $table = 'post_meta';
	
    public static function add_post_meta( $post_id, $meta_key, $meta_value ) {
		
		if( $post_id ) {
			if( $meta_value ) {
				$meta_value = is_array( $meta_value ) ? serialize( $meta_value ) : $meta_value; 
				
				$post_meta_data = array(
					'post_id'	=>	$post_id,
					'meta_key'		=>	$meta_key,
					'meta_value'	=>	$meta_value
				);
					
				$insert_post_meta = DB::table('post_meta')->insert( $post_meta_data );
				
				if( $insert_post_meta ){
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}
	
	public static function update_post_meta( $post_id, $meta_key, $meta_value ) {
			
		if( $post_id ) {
			$post_meta_exits = DB::table('post_meta')
				->where('post_id', $post_id) 
				->where('meta_key', $meta_key)
				->get();
			
			if( count( $post_meta_exits ) > 0 ) {
				
				$meta_value = is_array( $meta_value ) ? serialize( $meta_value ) : $meta_value; 
				$post_meta_data = array(
					'meta_value'	=>	$meta_value
				);
							
				$update_post_meta = DB::table('post_meta')
					->where('post_id', $post_id)
					->where('meta_key', $meta_key)
					->update($post_meta_data);
					
				if( $update_post_meta ){
					return true;
				} else {
					return false;
				}
				
			} else {
				return self::add_post_meta( $post_id, $meta_key, $meta_value );
				
			}
		}
	}
	
	public static function delete_post_meta( $post_id, $meta_key ) {
				
		if( $post_id ) {
			if( $meta_key ) {
				if( is_array( $meta_key ) ){
					foreach( $meta_key as $key ){
						DB::table('post_meta')
							->where('meta_key', $key) 
							->where('post_id', $post_id) 
							->delete();
					}
					
					return true;
					
				} else {
					DB::table('post_meta')
						->where('meta_key', $meta_key)
						->where('post_id', $post_id)
						->delete();
				
					$check_deleted = DB::table('post_meta') 
						->where('meta_key', $meta_key)
						->value('meta_key');
						
					if( count( $check_deleted ) == 0 ){
						return true;
					} else {
						return false;
					}
				}
				
			} else {
				return false;
			}
		}
	}
	
	/**
	 * get_post_meta():
	 *
	 * If the result of the get_post_meta() is serialized, you can use the unserialize() 
	 * function of PHP to convert the data back to array format.
	 * You can use is_serialized() helper function for checking weather the return 
	 * is serailized or not, the return of is_serialized() is boolean.
	 *
	 * @param post_id	ID of the item referenced from the "item" table
	 * @param meta_key		If left empty, it returns all meta values in array format
	 *
	 */
	public static function get_post_meta( $post_id, $meta_key = '' ) {
		
		if( $post_id ) {
			if( $meta_key ){
				$post_meta_data = DB::table('post_meta')
					->where('meta_key', $meta_key)
					->where('post_id', $post_id)
					->value('meta_value');
					
				if( $post_meta_data ){
					return $post_meta_data;
				} else {
					return false;
				}
				
			} else {
				$post_meta_data = DB::table('post_meta')
					->select('meta_key', 'meta_value')
					->where('post_id', $post_id)
					->get();
					
				$array_meta_data = array();
				
				if( $post_meta_data ){
					foreach( $post_meta_data as $key => $meta ){
						$array_meta_data[$meta->meta_key] = $meta->meta_value;
					}
					return (object) $array_meta_data;
				} else {
					return false;
				}
			}
		}
	}
}