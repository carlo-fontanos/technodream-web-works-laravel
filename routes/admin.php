<?php 

Route::group(['middleware' => ['auth']], function () {
	Route::group(['prefix' => 'admin'], function () {
		Route::get('/', 'Admin\AdminHomeController@index');
		Route::get('tests', 'Admin\AdminTestController@index');
		Route::group(['prefix' => 'tests'], function () {
			Route::post('pagination', 'Admin\AdminTestController@index_pagination');	
			Route::get('create', 'Admin\AdminTestController@create');
			Route::post('store', 'Admin\AdminTestController@store');
			Route::post('store-all', 'Admin\AdminTestController@store_all');
			Route::get('edit/{id}', 'Admin\AdminTestController@edit');
			Route::post('update', 'Admin\AdminTestController@update');
			Route::post('update-all', 'Admin\AdminTestController@update_all');
			Route::post('destroy/', 'Admin\AdminTestController@destroy');
			Route::get('{id}', 'Admin\AdminTestController@show');
		
		});
		
		Route::group(['prefix' => 'applicants'], function () {
			Route::get('/', 'Admin\AdminApplicantController@index');	
			Route::post('pagination', 'Admin\AdminApplicantController@index_pagination');	
			Route::post('update', 'Admin\AdminApplicantController@update');
			Route::get('show/{id}', 'Admin\AdminApplicantController@show');
			Route::get('edit/{id}', 'Admin\AdminApplicantController@edit');
			Route::post('destroy', 'Admin\AdminApplicantController@destroy');
		});
	});
});