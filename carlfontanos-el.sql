-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 04, 2018 at 09:16 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `root_exam`
--
CREATE DATABASE IF NOT EXISTS `root_exam` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `root_exam`;

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `answer_id` bigint(20) NOT NULL,
  `answer_content` varchar(191) DEFAULT NULL,
  `question_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`answer_id`, `answer_content`, `question_id`, `created_at`, `updated_at`) VALUES
(107, '298', 98, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(108, '301', 99, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(109, '302', 100, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(110, '305', 101, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(111, '306', 102, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(112, '308', 103, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(113, '311', 104, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(114, '313', 105, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(115, '315', 106, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(116, '317', 107, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(117, '318', 108, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(118, '322', 109, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(119, '328', 110, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(120, '333', 111, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(121, '334', 112, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(122, '340', 113, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(123, '343', 114, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(124, '349', 115, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(125, '350', 116, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(126, '355', 117, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(127, '359', 118, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(128, '364', 119, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(129, '371', 120, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(130, '374', 121, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(131, '379', 122, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(132, '387', 123, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(133, '390', 124, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(134, '396', 125, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(135, '399', 126, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(136, '402', 127, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(137, '406', 128, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(138, '411', 129, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(139, '417', 130, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(140, '422', 131, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(141, '425', 132, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(142, '429', 133, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(143, '433', 134, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(144, '440', 135, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(145, '444', 136, '2018-05-29 20:12:52', '2018-05-29 20:12:52'),
(153, '468', 154, '2018-06-04 08:37:24', '2018-06-04 08:37:24'),
(154, 'c1', 155, '2018-06-04 08:37:24', '2018-06-04 08:37:24');

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `applicant_id` int(10) NOT NULL,
  `department_id` bigint(20) NOT NULL,
  `test_id` bigint(20) NOT NULL,
  `first_name` varchar(191) NOT NULL,
  `middle_name` varchar(191) DEFAULT NULL,
  `last_name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `phone_number` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`applicant_id`, `department_id`, `test_id`, `first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `created_at`, `updated_at`) VALUES
(38, 4, 30, 'tes', NULL, 'test', 'tes@gmail.com', '35643435', '2018-06-01 11:21:42', '2018-06-01 11:21:42'),
(39, 1, 32, 'Jim', 'Garcia', 'Sison', 'jim_sison@gmail.com', '2456456', '2018-06-04 08:38:05', '2018-06-04 08:38:05'),
(40, 1, 32, 'Jake', 'Rossi', 'Manabat', 'jake_manabat@gmail.com', '56457678', '2018-06-04 08:42:54', '2018-06-04 08:42:54'),
(41, 1, 32, 'Greg', 'Adamson', 'Murphy', 'greg_murphy@gmail.com', '209384923', '2018-06-04 08:54:54', '2018-06-04 08:54:54');

-- --------------------------------------------------------

--
-- Table structure for table `choices`
--

CREATE TABLE `choices` (
  `choice_id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `choice_content` longtext,
  `choice_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `choices`
--

INSERT INTO `choices` (`choice_id`, `question_id`, `choice_content`, `choice_order`, `created_at`, `updated_at`) VALUES
(298, 98, 'is', 1, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(299, 98, 'are', 2, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(300, 99, 'write', 1, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(301, 99, 'writes', 2, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(302, 100, 'was', 1, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(303, 100, 'were', 2, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(304, 101, 'was', 1, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(305, 101, 'were', 2, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(306, 102, 'has', 1, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(307, 102, 'have', 2, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(308, 103, 'is', 1, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(309, 103, 'are', 2, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(310, 104, 'vote', 1, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(311, 104, 'votes', 2, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(312, 105, 'is', 1, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(313, 105, 'are', 2, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(314, 106, 'is', 1, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(315, 106, 'are', 2, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(316, 107, 'influence', 1, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(317, 107, 'influences', 2, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(318, 108, 'Exception', 1, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(319, 108, 'Standard', 2, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(320, 108, 'Yardstick', 3, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(321, 108, 'Guage', 4, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(322, 109, 'Inactive', 1, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(323, 109, 'Changing', 2, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(324, 109, 'Animated', 3, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(325, 109, 'Energetic', 4, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(326, 110, 'Contemporary', 1, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(327, 110, 'Ingenious', 2, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(328, 110, 'Customary', 3, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(329, 110, 'Original', 4, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(330, 111, 'Extend', 1, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(331, 111, 'Intensify', 2, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(332, 111, 'Amplify', 3, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(333, 111, 'Diminish', 4, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(334, 112, 'Loss', 1, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(335, 112, 'Increase', 2, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(336, 112, 'Addition', 3, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(337, 112, 'Augmentation', 4, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(338, 113, 'Affinity', 1, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(339, 113, 'Insight', 2, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(340, 113, 'Indifference', 3, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(341, 113, 'Compassion', 4, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(342, 114, 'Challenge', 1, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(343, 114, 'Acceptance', 2, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(344, 114, 'Criticism', 3, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(345, 114, 'Difficulty', 4, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(346, 115, 'Accede', 1, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(347, 115, 'Agree', 2, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(348, 115, 'Recognize', 3, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(349, 115, 'Renounce', 4, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(350, 116, 'Diffident', 1, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(351, 116, 'Decisive', 2, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(352, 116, 'Assured', 3, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(353, 116, 'Firm', 4, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(354, 117, 'Affinity', 1, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(355, 117, 'Coldness', 2, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(356, 117, 'Bond', 3, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(357, 117, 'Empathy', 4, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(358, 118, '10', 1, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(359, 118, '20', 2, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(360, 118, '30', 3, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(361, 118, '40', 4, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(362, 118, 'One cannot tell from the information given', 5, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(363, 119, '36 kilograms', 1, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(364, 119, '54 kilograms', 2, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(365, 119, '121 kilograms', 3, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(366, 119, '160 kilograms', 4, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(367, 119, '216 kilograms', 5, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(368, 120, '2 seconds', 1, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(369, 120, '9 seconds', 2, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(370, 120, '38 seconds', 3, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(371, 120, '45 seconds', 4, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(372, 120, '510 seconds', 5, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(373, 121, '5,000 units', 1, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(374, 121, '4,000 units', 2, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(375, 121, '2,000 units', 3, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(376, 121, '1,100 units', 4, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(377, 121, '1,000 units', 5, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(378, 122, '12.5 hours', 1, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(379, 122, '13.3 hours', 2, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(380, 122, '26.7 hours', 3, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(381, 122, '33.3 hours', 4, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(382, 122, '53.3 hours', 5, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(383, 123, '$6', 1, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(384, 123, '$10', 2, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(385, 123, '$3', 3, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(386, 123, '$4', 4, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(387, 123, '$5', 5, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(388, 124, '307 employees', 1, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(389, 124, '318 employees', 2, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(390, 124, '324 employees', 3, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(391, 124, '326 employees', 4, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(392, 124, '343 employees', 5, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(393, 125, 'If an organization’s profitability is not in jeopardy then the competence of its purchasing function will not determine whether it operates at a profit or at a loss.', 1, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(394, 125, 'There are at least some purchasing functions that are not responsible for a significant amount of an organization’s profit.', 2, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(395, 125, 'A non-purchasing function will not bear significant responsibility for the profit of an organization.', 3, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(396, 125, 'An organization whose profitability is in jeopardy may depend on the efficiency and skill of its purchasing function to determine whether it operates at a profit or at a loss.', 4, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(397, 126, 'An organizational function determines the physical form of a product if and only if it addresses mechanical and electrical issues.', 1, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(398, 126, 'There are product development projects in which the physical form of the product need not be determined before target prices are set.', 2, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(399, 126, 'Whenever an organization is determining the physical form of a developing product, it is performing a design function.', 3, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(400, 126, 'There are at least some product development projects in which the marketing function does not set target prices or launch and promote the product.', 4, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(401, 127, 'There are individual instances in which authority rests to a significant extent on the  personal characteristics of a manager.', 1, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(402, 127, 'All managers who supervise employees have the right to authority.', 2, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(403, 127, 'If an individual no longer has authority, then he or she recently vacated a managerial position that involved employee supervision.', 3, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(404, 127, 'An individual who has a right to authority is often not the incumbent of a managerial position in charge of supervising employees.', 4, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(405, 128, 'If an organization experiences difficulty balancing the competing desires of stakeholder groups then it is almost certain that the organization operates across multiple countries.', 1, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(406, 128, 'No organization that is able to survive can do so without satisfying the desires of several different stakeholder groups.', 2, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(407, 128, 'There are at least some organizations that operate across multiple countries but do not experience difficulty balancing the competing desires of stakeholder groups', 3, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(408, 128, 'No organization that satisfies the desires of all its stakeholders will fail to survive.', 4, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(409, 129, 'There are at least some automated systems that require considerable human involvement to perform mechanical operations', 1, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(410, 129, 'If an organization can consistently produce a high quality product, it is unlikely that it will use an automated system.', 2, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(411, 129, 'No hard automation system is more adaptable than a soft automation system.', 3, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(412, 129, 'Any organization that uses an automated system has found its work to be monotonous or unsafe for employees.', 4, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(413, 130, 'A', 1, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(414, 130, 'B', 2, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(415, 130, 'C', 3, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(416, 130, 'D', 4, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(417, 130, 'E', 5, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(418, 131, 'A', 1, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(419, 131, 'B', 2, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(420, 131, 'C', 3, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(421, 131, 'D', 4, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(422, 131, 'E', 5, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(423, 132, 'A', 1, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(424, 132, 'B', 2, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(425, 132, 'C', 3, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(426, 132, 'D', 4, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(427, 132, 'E', 5, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(428, 133, 'A', 1, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(429, 133, 'B', 2, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(430, 133, 'C', 3, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(431, 133, 'D', 4, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(432, 133, 'E', 5, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(433, 134, 'A', 1, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(434, 134, 'B', 2, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(435, 134, 'C', 3, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(436, 134, 'D', 4, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(437, 134, 'E', 5, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(438, 135, 'A', 1, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(439, 135, 'B', 2, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(440, 135, 'C', 3, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(441, 135, 'D', 4, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(442, 135, 'E', 5, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(443, 136, 'A', 1, '2018-05-29 20:12:51', '2018-05-29 20:12:51'),
(444, 136, 'B', 2, '2018-05-29 20:12:52', '2018-05-29 20:12:52'),
(445, 136, 'C', 3, '2018-05-29 20:12:52', '2018-05-29 20:12:52'),
(446, 136, 'D', 4, '2018-05-29 20:12:52', '2018-05-29 20:12:52'),
(447, 136, 'E', 5, '2018-05-29 20:12:52', '2018-05-29 20:12:52'),
(466, 154, 'c1', 1, '2018-06-04 08:37:24', '2018-06-04 08:37:24'),
(467, 154, 'c2', 2, '2018-06-04 08:37:24', '2018-06-04 08:37:24'),
(468, 154, 'c3', 3, '2018-06-04 08:37:24', '2018-06-04 08:37:24'),
(469, 154, 'c4', 4, '2018-06-04 08:37:24', '2018-06-04 08:37:24');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `department_id` bigint(20) NOT NULL,
  `department_name` varchar(191) NOT NULL,
  `test_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_id`, `department_name`, `test_id`, `created_at`, `updated_at`) VALUES
(1, 'Programming', NULL, NULL, NULL),
(2, 'SEO', NULL, NULL, NULL),
(4, 'Call Center / Human Resource / Sales', NULL, NULL, NULL),
(6, 'Designers', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_04_10_111027_create_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `postmeta`
--

CREATE TABLE `postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `post_password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `question_content` longtext,
  `question_points` int(11) DEFAULT NULL,
  `question_type` varchar(191) NOT NULL,
  `question_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `section_id`, `question_content`, `question_points`, `question_type`, `question_order`, `created_at`, `updated_at`) VALUES
(98, 64, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">The rhythm of the pounding waves _____ calming.</span></span></span></span></p>', 1, 'multiple', 1, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(99, 64, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">John or Doris _______ us regularly</span></span></span></span></p>', 1, 'multiple', 2, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(100, 64, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Neither the apples nor the basket _________ expensive.</span></span></span></span></p>', 1, 'multiple', 3, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(101, 64, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Hardest hit by the drought _______ the farmers.</span></span></span></span></p>', 1, 'multiple', 4, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(102, 64, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Each of them ______ a good seat.</span></span></span></span></p>', 1, 'multiple', 5, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(103, 64, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Ten million gallons of oil _____ a lot.</span></span></span></span></p>', 1, 'multiple', 6, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(104, 64, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">The jury ________ today.</span></span></span></span></p>', 1, 'multiple', 7, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(105, 64, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">The majority of us ______ in favor.</span></span></span></span></p>', 1, 'multiple', 8, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(106, 64, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Statistics _____ often misleading.</span></span></span></span></p>', 1, 'multiple', 9, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(107, 64, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">A high tax, not to mention unemployment, _____ votes.</span></span></span></span></p>', 1, 'multiple', 10, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(108, 65, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Which word does <u>not</u> have a similar meaning to - BENCHMARK</span></span></span></span></p>', 1, 'multiple', 1, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(109, 65, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Which word does <u>not</u> have a similar meaning to - DYNAMIC</span></span></span></span></p>', 1, 'multiple', 2, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(110, 65, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Which word does <u>not</u> have a similar meaning to - INNOVATIVE</span></span></span></span></p>', 1, 'multiple', 3, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(111, 65, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Which word does <u>not</u> have a similar meaning to - ESCALATE</span></span></span></span></p>', 1, 'multiple', 4, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(112, 65, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Which word does <u>not</u> have a similar meaning to - INCREMENT</span></span></span></span></p>', 1, 'multiple', 5, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(113, 65, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Which word does <u>not</u> have a similar meaning to - EMPATHY</span></span></span></span></p>', 1, 'multiple', 6, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(114, 65, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Which word does <u>not</u> have a similar meaning to - OBJECTION</span></span></span></span></p>', 1, 'multiple', 7, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(115, 65, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Which word does <u>not</u> have a similar meaning to - ACKNOWLEDGE</span></span></span></span></p>', 1, 'multiple', 8, '2018-05-29 20:12:46', '2018-05-29 20:12:46'),
(116, 65, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Which word does <u>not</u> have a similar meaning to - ASSERTIVE</span></span></span></span></p>', 1, 'multiple', 9, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(117, 65, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Which word does <u>not</u> have a similar meaning to - RAPPORT</span></span></span></span></p>', 1, 'multiple', 10, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(118, 66, '<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Two trucks were driven on a 1,680 kilometer (km) trip. The first truck averaged 14 km per liter of fuel for the trip, and the second averaged 12 km per liter. The second truck used how many more liters of gas than the first? </span></span></span></span></span></span></p>\n\n<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:144.0pt; text-align:justify; margin:0cm 0cm 10pt\">&nbsp;</p>', 1, 'multiple', 1, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(119, 66, '<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">A warehouse&#39;s rectangular ceiling measures 12 meters by 18 meters, and is covered by acoustic tile squares which are one meter on a side. Each piece of tile weighs a quarter of a kilogram. What will be the total weight of the tile required to cover the ceiling? </span></span></span></span></span></span></p>', 1, 'multiple', 2, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(120, 66, '<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">The elevator of a high-rise office building provides access to all floors. If each floor is 5 meters high and the elevator travels at a rate of 7.5 meters per second, approximately how long will it take to travel from the first floor to the sixty-eighth floor? </span></span></span></span></span></span></p>', 1, 'multiple', 3, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(121, 66, '<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">In 2001, a company marketed 730,000 units of its product. In 2001 its yearly volume was 50% of its volume for 2004. The 2004 volume represents how many units for each of the 365 days of 2004? </span></span></span></span></span></span></p>', 1, 'multiple', 4, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(122, 66, '<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">A cargo ship travels at an average speed of 25 kilometers per hour while traveling 500 kilometers from port A to port B. During the return trip along the same route from port B to port A, the cargo ship travels at an average speed of 15 kilometers per hour. What is the approximate difference in the travel times between the two trips? </span></span></span></span></span></span></p>', 1, 'multiple', 5, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(123, 66, '<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">A book is available at a local store for $20 in the hard cover edition or $10 in paperback. The book can also be obtained from a mail-order service at 30% off list, plus $1 for postage and handling. How much more will it cost to buy the hard cover edition mail order than to buy the paperback edition at the local store? </span></span></span></span></span></span></p>\n\n<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"tab-stops:429.0pt\"><span style=\"font-family:Calibri,sans-serif\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></span></p>', 1, 'multiple', 6, '2018-05-29 20:12:48', '2018-05-29 20:12:48'),
(124, 66, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">In January, the total number of employees at a factory was reduced by 8%. In July, the demand for the factory&rsquo;s product increased so the total number of employees was increased by 8%. The total number of employees at the factory in January, before the number of employees was reduced, was 326. What was the total number of employees at the factory after the number of employees was increased in July? </span></span></span></span></p>', 1, 'multiple', 7, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(125, 67, '<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Purchasing can have a significant effect on an organization&rsquo;s total profit. However, the success of a purchasing &nbsp;&nbsp;&nbsp;function relies on competent buyers and a purchasing manager who employs systematic purchasing methods and implements technological advances. If an organization&rsquo;s profitability is in jeopardy, the efficiency and skill of its purchasing function may determine whether it operates at a profit or at a loss. As such, the purchasing function bears a significant amount of the responsibility for an organization&rsquo;s profit, and, whenever an organization strives to produce profit, it will expend the effort required to hire capable and qualified buyers as well as a knowledgeable, intelligent purchasing manager. </span></span></span></span></span></span></p>\n\n<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">From the information given above, it can be validly concluded that: </span></span></span></span></span></span></p>', 1, 'multiple', 1, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(126, 67, '<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">There are three central organizational functions to every product development project: marketing, design, and manufacturing. The marketing function consists of the interactions between the organization and the customers, which includes setting target prices and overseeing the launch and promotion of a new product. The design function determines the physical form of the product. This includes the engineering design, such as mechanical and electrical issues, as well as the industrial design, which includes aesthetics and user interfaces. The manufacturing function is responsible for designing and operating the system for producing the product. This function includes purchasing, distribution, and installation. </span></span></span></span></span></span></p>\n\n<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">From the information given above, it can be validly concluded that: </span></span></span></span></span></span></p>', 1, 'multiple', 2, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(127, 67, '<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">If a manager supervises employees, he or she has several specific rights that are acquired from the managerial rank. One of these rights is authority. A manager with authority has the right to give orders to subordinates. This authority relates to the position itself and has nothing to do with the personal characteristics of the individual manager. When a position of authority is vacated, the person who left the position no longer has authority. The authority remains with the position and the new manager. </span></span></span></span></span></span></p>\n\n<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">From the information given above, it can be validly concluded that: </span></span></span></span></span></span></p>', 1, 'multiple', 3, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(128, 67, '<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">In order for an organization to survive, it must satisfy several different groups of stakeholders. Stakeholders typically include stockholders, employees, customers, and society in general. Unfortunately, the desires of stakeholder groups are often in conflict. In the long run, the desires of all of these groups must be satisfied adequately by the organization, or none of them will be satisfied at all. This is because any one of these groups is strong enough to cause the downfall of the organization if their desires are not satisfied. When an organization operates within a single country, balancing the competing desires of stakeholder groups is sometimes difficult. For those organizations that operate across multiple countries, where dominant interests often vary widely, balancing these competing desires is always difficult. </span></span></span></span></span></span></p>\n\n<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">From the information given above, it can be validly concluded that: </span></span></span></span></span></span></p>', 1, 'multiple', 4, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(129, 67, '<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">Mechanization is defined as the process of using machines to perform work that had previously been performed by people. Automation, which is an extension of mechanization, is defined as the process of performing mechanical operations with minimal or no human involvement. Automation is commonly used whenever an organization faces difficulties producing a product of a consistent quality, as well as when work is monotonous or unsafe for employees. Automation systems can be either hard or soft. Soft automation systems are adaptable, thus allowing them to perform several different functions. Hard automation systems have each machine perform one specific function. As a result, these systems are less adaptable than soft automation systems. </span></span></span></span></span></span></p>\n\n<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">From the information given above, it can be validly concluded that: </span></span></span></span></span></span></p>\n\n<p style=\"margin-top:0cm; margin-right:0cm; margin-bottom:.0001pt; margin-left:36.0pt; text-align:justify; margin:0cm 0cm 10pt\">&nbsp;</p>', 1, 'multiple', 5, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(130, 68, '<p><img height=\"118\" src=\"http://192.168.2.223/public/uploads/ckfinder/images/image(2).png\" width=\"293\" /></p>', 1, 'multiple', 1, '2018-05-29 20:12:50', '2018-05-30 12:07:08'),
(131, 68, '<p><img height=\"188\" src=\"http://192.168.2.223/public/uploads/ckfinder/images/image(3).png\" width=\"287\" /></p>', 1, 'multiple', 2, '2018-05-29 20:12:50', '2018-05-30 12:07:01'),
(132, 68, '<p><img height=\"184\" src=\"http://192.168.2.223/public/uploads/ckfinder/images/image(4).png\" width=\"288\" /></p>', 1, 'multiple', 3, '2018-05-29 20:12:51', '2018-05-30 12:06:55'),
(133, 68, '<p><img height=\"110\" src=\"http://192.168.2.223/public/uploads/ckfinder/images/image(5).png\" width=\"281\" /></p>', 1, 'multiple', 4, '2018-05-29 20:12:51', '2018-05-30 12:06:45'),
(134, 68, '<p><img height=\"121\" src=\"http://192.168.2.223/public/uploads/ckfinder/images/image(6).png\" width=\"280\" /></p>', 1, 'multiple', 5, '2018-05-29 20:12:51', '2018-05-30 12:06:36'),
(135, 68, '<p><img height=\"138\" src=\"http://192.168.2.223/public/uploads/ckfinder/images/image(7).png\" width=\"296\" /></p>', 1, 'multiple', 6, '2018-05-29 20:12:51', '2018-05-30 12:06:23'),
(136, 68, '<p><img height=\"189\" src=\"http://192.168.2.223/public/uploads/ckfinder/images/image(8).png\" width=\"278\" /></p>', 1, 'multiple', 7, '2018-05-29 20:12:51', '2018-05-30 12:06:08'),
(137, 69, '<p>What actions of yours prove that you take initiative at work?</p>', 3, 'essay', 1, '2018-05-29 21:06:47', '2018-05-29 21:07:18'),
(138, 69, '<p>How do you adapt to major changes at work?</p>', 3, 'essay', 2, '2018-05-29 21:07:21', '2018-05-29 21:07:36'),
(139, 69, '<p>Describe a recent experience/ project that you found enjoyable and demanding.</p>', 3, 'essay', 3, '2018-05-29 21:07:40', '2018-05-29 21:08:09'),
(140, 69, '<p>Ca you describe an experience where you were asked to prepare a report on something?</p>', 3, 'essay', 4, '2018-05-29 21:08:18', '2018-05-29 21:08:49'),
(141, 69, '<p>Why do you think that man holes are round in shape?</p>', 3, 'essay', 5, '2018-05-29 21:08:21', '2018-05-29 21:10:57'),
(142, 69, '<p>How do you think your skills and experiences match the job we offer?</p>', 3, 'essay', 6, '2018-05-29 21:08:24', '2018-05-29 21:09:27'),
(143, 69, '<p>Describe a win-win situation that you have negotiated.</p>', 3, 'essay', 7, '2018-05-29 21:08:26', '2018-05-29 21:10:17'),
(144, 69, '<p>Why do you think our organization can be the right employer for you?</p>', 3, 'essay', 8, '2018-05-29 21:08:27', '2018-05-29 21:10:52'),
(154, 75, '<p>q1</p>', 1, 'multiple', 1, '2018-06-04 08:37:24', '2018-06-04 08:37:24'),
(155, 76, '<p>q1</p>', 5, 'input', 1, '2018-06-04 08:37:24', '2018-06-04 08:37:24');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` bigint(20) NOT NULL,
  `applicant_id` bigint(20) NOT NULL,
  `department_id` bigint(20) NOT NULL,
  `test_id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `choice_id` bigint(20) DEFAULT NULL,
  `points` int(11) NOT NULL DEFAULT '0',
  `answer_content` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`id`, `applicant_id`, `department_id`, `test_id`, `question_id`, `choice_id`, `points`, `answer_content`, `created_at`, `updated_at`) VALUES
(296, 38, 4, 30, 98, 298, 1, NULL, '2018-06-01 12:39:38', '2018-06-01 12:39:38'),
(297, 38, 4, 30, 99, 301, 1, NULL, '2018-06-01 12:39:43', '2018-06-01 12:39:43'),
(298, 38, 4, 30, 100, 302, 1, NULL, '2018-06-01 12:39:45', '2018-06-01 12:39:45'),
(299, 38, 4, 30, 101, 305, 1, NULL, '2018-06-01 12:39:49', '2018-06-01 12:39:49'),
(300, 38, 4, 30, 102, 307, 0, NULL, '2018-06-01 12:39:52', '2018-06-01 12:39:52'),
(301, 38, 4, 30, 103, 309, 0, NULL, '2018-06-01 12:39:55', '2018-06-01 12:39:55'),
(302, 38, 4, 30, 104, 311, 1, NULL, '2018-06-01 12:39:57', '2018-06-01 12:39:57'),
(303, 38, 4, 30, 105, 313, 1, NULL, '2018-06-01 12:39:59', '2018-06-01 12:39:59'),
(304, 38, 4, 30, 106, 314, 0, NULL, '2018-06-01 12:40:07', '2018-06-01 12:40:07'),
(305, 38, 4, 30, 107, 317, 1, NULL, '2018-06-01 12:40:12', '2018-06-01 12:40:12'),
(306, 38, 4, 30, 108, 318, 1, NULL, '2018-06-01 12:40:19', '2018-06-01 12:40:19'),
(307, 38, 4, 30, 109, 322, 1, NULL, '2018-06-01 12:40:25', '2018-06-01 12:40:25'),
(308, 38, 4, 30, 110, 329, 0, NULL, '2018-06-01 12:40:30', '2018-06-01 12:40:30'),
(309, 38, 4, 30, 111, 330, 0, NULL, '2018-06-01 12:40:34', '2018-06-01 12:40:34'),
(310, 38, 4, 30, 112, 336, 0, NULL, '2018-06-01 12:40:36', '2018-06-01 12:40:36'),
(311, 38, 4, 30, 113, 341, 0, NULL, '2018-06-01 12:40:38', '2018-06-01 12:40:38'),
(312, 38, 4, 30, 114, 343, 1, NULL, '2018-06-01 12:40:40', '2018-06-01 12:40:40'),
(313, 38, 4, 30, 115, 346, 0, NULL, '2018-06-01 12:40:42', '2018-06-01 12:40:42'),
(314, 38, 4, 30, 116, 351, 0, NULL, '2018-06-01 12:40:43', '2018-06-01 12:40:43'),
(315, 38, 4, 30, 117, 355, 1, NULL, '2018-06-01 12:40:46', '2018-06-01 12:40:46'),
(316, 38, 4, 30, 118, 358, 0, NULL, '2018-06-01 12:40:47', '2018-06-01 12:40:47'),
(317, 38, 4, 30, 119, 364, 1, NULL, '2018-06-01 12:40:49', '2018-06-01 12:40:49'),
(318, 38, 4, 30, 120, 369, 0, NULL, '2018-06-01 12:40:51', '2018-06-01 12:40:51'),
(319, 38, 4, 30, 121, 375, 0, NULL, '2018-06-01 12:40:53', '2018-06-01 12:40:53'),
(320, 38, 4, 30, 122, 379, 1, NULL, '2018-06-01 12:40:56', '2018-06-01 12:40:56'),
(321, 38, 4, 30, 123, 384, 0, NULL, '2018-06-01 12:40:59', '2018-06-01 12:40:59'),
(322, 38, 4, 30, 124, 392, 0, NULL, '2018-06-01 12:41:01', '2018-06-01 12:41:01'),
(323, 38, 4, 30, 125, 394, 0, NULL, '2018-06-01 12:41:03', '2018-06-01 12:41:03'),
(324, 38, 4, 30, 126, 399, 1, NULL, '2018-06-01 12:41:05', '2018-06-01 12:41:05'),
(325, 38, 4, 30, 127, 404, 0, NULL, '2018-06-01 12:41:07', '2018-06-01 12:41:07'),
(326, 38, 4, 30, 128, 406, 1, NULL, '2018-06-01 12:41:10', '2018-06-01 12:41:10'),
(327, 38, 4, 30, 129, 410, 0, NULL, '2018-06-01 12:41:12', '2018-06-01 12:41:12'),
(328, 38, 4, 30, 130, 417, 1, NULL, '2018-06-01 12:41:15', '2018-06-01 12:41:15'),
(329, 38, 4, 30, 131, 422, 1, NULL, '2018-06-01 12:41:19', '2018-06-01 12:41:19'),
(330, 38, 4, 30, 132, 425, 1, NULL, '2018-06-01 12:41:22', '2018-06-01 12:41:22'),
(331, 38, 4, 30, 133, 429, 1, NULL, '2018-06-01 12:41:26', '2018-06-01 12:41:26'),
(332, 38, 4, 30, 134, 433, 1, NULL, '2018-06-01 12:41:29', '2018-06-01 12:41:29'),
(333, 38, 4, 30, 135, 440, 1, NULL, '2018-06-01 12:41:31', '2018-06-01 12:41:31'),
(334, 38, 4, 30, 136, 444, 1, NULL, '2018-06-01 12:41:33', '2018-06-01 12:41:33'),
(335, 38, 4, 30, 137, NULL, 0, 'vb', '2018-06-01 12:41:41', '2018-06-01 12:41:41'),
(336, 38, 4, 30, 138, NULL, 0, 'jl', '2018-06-01 12:41:47', '2018-06-01 12:41:47'),
(337, 38, 4, 30, 139, NULL, 0, 'kjl', '2018-06-01 12:41:49', '2018-06-01 12:41:49'),
(338, 38, 4, 30, 140, NULL, 0, 'j', '2018-06-01 12:41:51', '2018-06-01 12:41:51'),
(339, 38, 4, 30, 141, NULL, 0, 'j', '2018-06-01 12:41:53', '2018-06-01 12:41:53'),
(340, 38, 4, 30, 142, NULL, 0, 'j', '2018-06-01 12:41:55', '2018-06-01 12:41:55'),
(341, 38, 4, 30, 143, NULL, 0, 'k', '2018-06-01 12:42:09', '2018-06-01 12:42:09'),
(342, 38, 4, 30, 144, NULL, 0, 'k', '2018-06-01 12:42:10', '2018-06-01 12:42:10'),
(343, 39, 1, 32, 154, 467, 0, NULL, '2018-06-04 08:38:09', '2018-06-04 08:38:09'),
(344, 39, 1, 32, 155, NULL, 5, 'c1', '2018-06-04 08:38:16', '2018-06-04 08:38:16'),
(345, 40, 1, 32, 154, 469, 0, NULL, '2018-06-04 08:47:44', '2018-06-04 08:47:44'),
(346, 40, 1, 32, 155, NULL, 0, 'd', '2018-06-04 08:47:47', '2018-06-04 08:47:47'),
(347, 41, 1, 32, 154, 468, 1, NULL, '2018-06-04 08:55:24', '2018-06-04 08:55:24'),
(348, 41, 1, 32, 155, NULL, 0, 'test', '2018-06-04 08:55:28', '2018-06-04 08:55:28');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `section_id` bigint(20) NOT NULL,
  `test_id` bigint(20) NOT NULL,
  `section_name` text,
  `section_content` longtext,
  `section_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`section_id`, `test_id`, `section_name`, `section_content`, `section_order`, `created_at`, `updated_at`) VALUES
(64, 30, 'Subject-Verb Agreement', '<p>Choose the best answer.</p>', 1, '2018-05-29 20:12:44', '2018-05-29 20:12:44'),
(65, 30, 'Word Meaning and Anologies', '<p>Choose the best answer.</p>', 2, '2018-05-29 20:12:45', '2018-05-29 20:12:45'),
(66, 30, 'Numerical Reasoning', '<p style=\"margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:115%\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">The following questions are designed to assess your ability to use numerical information to solve complex business related problems. Each question will present you with a short description of a real life situation. </span></span></span></span></span></span></span></p>', 3, '2018-05-29 20:12:47', '2018-05-29 20:12:47'),
(67, 30, 'Logic Based Reasoning', '<p style=\"margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:115%\"><span style=\"font-family:Calibri,sans-serif\"><b><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">The following questions are designed to assess your ability to think logically. In each question, you will be presented with a paragraph of information and four (4) response options. </span></span></span></span></b></span></span></span></p>\n\n<p style=\"margin-bottom:.0001pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><b><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">To answer each question, read through the information contained in the paragraph. You will then be asked to select the one option that represents either: </span></span></span></b></span></span></span></p>\n\n<p style=\"margin-bottom:.0001pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">1. The <b>only valid</b> statement that can be logically concluded from the information provided in the paragraph, or </span></span></span></span></span></span></p>\n\n<p style=\"margin-bottom:.0001pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">2. The <b>only invalid</b> statement that cannot be logically concluded from the information provided in the paragraph. </span></span></span></span></span></span></p>\n\n<p style=\"margin-bottom:.0001pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">To identify the correct answer, it is essential that you use ONLY the information provided in the paragraph.</span></span></span></span></span></span></p>\n\n<p style=\"margin-bottom:.0001pt; text-align:justify; margin:0cm 0cm 10pt\">&nbsp;</p>', 4, '2018-05-29 20:12:49', '2018-05-29 20:12:49'),
(68, 30, 'Figural Reasoning', '<p style=\"margin-bottom:.0001pt; text-align:justify; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><b><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">The following questions are designed to assess your ability to solve novel problems. Shown below are the two types of questions you will complete in this Section. Each question will present you with a set of boxes. In this set of boxes, one box will contain a question mark (?) and all other boxes will contain figures or shapes. </span></span></span></b></span></span></span></p>\n\n<p style=\"margin-bottom:.0001pt; margin:0cm 0cm 10pt\"><span style=\"font-size:11pt\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Book Antiqua&quot;,serif\"><span style=\"color:black\">The figures and shapes in each set of boxes combine to form a pattern. Your task is to determine which of the five (5) options should replace the question mark (?) to complete the pattern.</span></span></span></span></span></span></p>', 5, '2018-05-29 20:12:50', '2018-05-29 20:12:50'),
(69, 30, 'Essay', NULL, 6, '2018-05-29 21:05:07', '2018-05-29 21:06:41'),
(75, 32, 'sec 1', '<p>sec 1 desc</p>', 1, '2018-06-04 08:37:23', '2018-06-04 08:37:23'),
(76, 32, 'sec 2', '<p>sec 2&nbsp;desc</p>', 2, '2018-06-04 08:37:24', '2018-06-04 08:37:24');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `test_id` bigint(20) NOT NULL,
  `department_id` bigint(20) NOT NULL,
  `test_name` varchar(191) DEFAULT NULL,
  `test_content` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`test_id`, `department_id`, `test_name`, `test_content`, `created_at`, `updated_at`) VALUES
(30, 4, 'General Aptitude Test', '<p>An&nbsp;<strong><a href=\"http://www.jobtestprep.net/affiliates/traffic.php?id=23&amp;tid1=wikijob&amp;tid2=aptitude-tests-copy&amp;url=https://bit.ly/17DE3yZ\" rel=\"nofollow\" target=\"_blank\">aptitude test</a></strong>&nbsp;is a systematic means of testing a job candidate&#39;s abilities to perform specific tasks and react to a range of different situations.&nbsp; No prior knowledge is assumed, as the tests seek to determine innate ability at a particular competency.</p>', '2018-05-29 20:12:43', '2018-06-01 12:38:10'),
(32, 1, 'Test Programming', NULL, '2018-06-04 08:37:23', '2018-06-04 08:55:20');

-- --------------------------------------------------------

--
-- Table structure for table `usermeta`
--

CREATE TABLE `usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Carl Victor Fontanos', 'carl.esilverconnect@gmail.com', '$2y$10$Mo8uNdxiyctv4Wf685T3F.772TLEcPdav5lj8thh8w1Z5pyed35EC', 'gclfAlRW7SKej7UquUhZRnpVeMcxTWJf7CS0VDruQiIVmfJ09fgrHaNgqrJ7', '2018-04-15 19:18:39', '2018-04-15 19:18:39'),
(3, 'HR', 'hr@technodreamwebworks.com', '$2y$10$TCDTj0sdm8o8ruDyfu0tuuUzf/Sx.LgUOGdJYw0SUhjAQZXhd7kbG', 'HNPpYTzrkNPyKKVlCoFpSFzJkNtuXDPtmWzCyvfUSYskj5S798VAQkzx4ItQ', '2018-05-27 06:20:08', '2018-05-27 06:20:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`answer_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`applicant_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`choice_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `postmeta`
--
ALTER TABLE `postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `section_id` (`section_id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`section_id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`test_id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `usermeta`
--
ALTER TABLE `usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `answer_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `applicant_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `choices`
--
ALTER TABLE `choices`
  MODIFY `choice_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=470;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `department_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `postmeta`
--
ALTER TABLE `postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;
--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=349;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `section_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `test_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `usermeta`
--
ALTER TABLE `usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `applicants`
--
ALTER TABLE `applicants`
  ADD CONSTRAINT `applicants_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `departments` (`department_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `applicants_ibfk_4` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`) ON DELETE NO ACTION;

--
-- Constraints for table `choices`
--
ALTER TABLE `choices`
  ADD CONSTRAINT `choices_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_6` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tests`
--
ALTER TABLE `tests`
  ADD CONSTRAINT `tests_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `departments` (`department_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
