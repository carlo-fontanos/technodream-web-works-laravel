<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
	<link rel="shortcut icon" type="image/png" href="{{ url('public/img/favicon.ico') }}"/>
	
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/skin/plugins/bootstrap/css/bootstrap.min.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/skin/css/AdminLTE.min.css') }}">
	<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/skin/css/skins/skin-green.min.css') }}">
	<!-- iCheck -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/skin/plugins/iCheck/flat/blue.css') }}">
	<!-- Morris chart -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/skin/plugins/morris/morris.css') }}">
	<!-- jvectormap -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/skin/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/skin/plugins/datepicker/datepicker3.css') }}">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/skin/plugins/daterangepicker/daterangepicker.css') }}">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/skin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
	<!-- lobibox -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/plugins/lobibox/css/lobibox.min.css') }}">	
	
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="//oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<!-- Common Overrides -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/css/overrides.css') }}">
	<!-- Main Styles / AdminLTE Overrides -->
	<link rel="stylesheet" href="{{ url('public/theme/admin/css/styles.css') }}">
</head>
<body class="hold-transition skin-green sidebar-mini override">
	<div class="wrapper">
		<header class="main-header">
			<!-- Logo -->
			<a href="{{ URL('/admin') }}" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>A</b></span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Admin</b></span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
				</a>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="{{ url('/public/theme/admin/img/user-placeholder.jpg') }}" class="user-image" alt="User Image">
								<span class="hidden-xs">Administrator</span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="{{ url('/public/theme/admin/img/user-placeholder.jpg') }}" class="img-circle" alt="User Image">
									<p>
										Administrator
									</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-right">
										<a href="#" class="btn btn-default btn-flat" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
						<li>
							<a href="#" class="bg-green-active" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i></a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="{{ url('/public/theme/admin/img/user-placeholder.jpg') }}" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Administrator</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="{{ set_active('admin') }}">
						<a href="{{ url('/admin') }}">
							<i class="fa fa-pie-chart"></i> <span>Dashboard</span>
						</a>
					</li>
					<li class="treeview {{ set_active(['admin/tests', Request::is('admin/tests/*')]) }}">
						<a href="#">
							<i class="fa fa fa-table"></i>
							<span>Tests</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class = "{{ set_active('admin/tests') }}">
								<a href="{{ url('/admin/tests/') }}">
									<i class="fa fa-circle-o"></i> All
								</a>
							</li>
							<li class = "{{ set_active('admin/tests/create') }}">
								<a href="{{ url('/admin/tests/create') }}">
									<i class="fa fa-circle-o"></i> Add New
								</a>
							</li>
						</ul>
					</li>
					<li class="{{ set_active(['admin/applicants', Request::is('admin/applicants/*')]) }}">
						<a href="{{ url('/admin/applicants/') }}">
							<i class="fa fa-users"></i> <span>Applicants</span>
						</a>
					</li>
					<li>
						<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
							<i class="fa fa-circle-o text-red"></i> <span>Sign Out</span>
						</a>
					</li>

					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>
		
		<div class="content-wrapper">
			@yield('content')
		</div>
		
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; <?php echo date('Y'); ?> <a href="http://technodreamwebworks.com">Technodream Web Works</a>.</strong> All rights
			reserved.
		</footer>
	</div>
	
	@include('common.variables')
	
	<!-- jQuery 2.2.3 -->
	<script src="{{ url('public/theme/admin/skin/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.6 -->
	<script src="{{ url('public/theme/admin/skin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
	<!-- Morris.js charts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="{{ url('public/theme/admin/skin/plugins/morris/morris.min.js') }}"></script>
	<!-- Sparkline -->
	<script src="{{ url('public/theme/admin/skin/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
	<!-- jvectormap -->
	<script src="{{ url('public/theme/admin/skin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
	<script src="{{ url('public/theme/admin/skin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
	<!-- jQuery Knob Chart -->
	<script src="{{ url('public/theme/admin/skin/plugins/knob/jquery.knob.js') }}"></script>
	<!-- daterangepicker -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script src="{{ url('public/theme/admin/skin/plugins/daterangepicker/daterangepicker.js') }}"></script>
	<!-- datepicker -->
	<script src="{{ url('public/theme/admin/skin/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{ url('public/theme/admin/skin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
	<!-- Slimscroll -->
	<script src="{{ url('public/theme/admin/skin/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
	<!-- FastClick -->
	<script src="{{ url('public/theme/admin/skin/plugins/fastclick/fastclick.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ url('public/theme/admin/skin/js/app.min.js') }}"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="{{ url('public/theme/admin/skin/js/pages/dashboard.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ url('public/theme/admin/skin/js/demo.js') }}"></script>
	<!--  CKEditor -->
	<script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
	 <!--  Jquery Form Plugin -->
	<script src="{{ url('public/theme/admin/plugins/jquery-form/jquery.form.js') }}"></script>
	<!-- Lobibox -->
	<script src="{{ url('public/theme/admin/plugins/lobibox/js/lobibox.min.js') }}"></script>
	
	<!-- Custom Scripts -->
	<script src="{{ url('public/theme/admin/js/global.js') }}"></script>
	<script src="{{ url('public/theme/admin/js/app.js') }}"></script>
</body>
</html>
