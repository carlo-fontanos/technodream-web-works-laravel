@extends('admin.layouts.app')

@section('content')

<div class="admin-home">
	<section class="content-header">
		<h1>Dashboard <small>Control panel</small></h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>
	 
	<section class="content">
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>{{ $applicants_day }}</h3>
						<p>Applicants Today</p>
					</div>
					<div class="icon">
						<i class="ion ion-android-calendar"></i>
					</div>
					<a href="{{ url('/admin/applicants') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>

			<div class="col-lg-3 col-xs-6">
				<div class="small-box bg-green">
					<div class="inner">
						<h3>{{ $applicants_week }}</h3>
						<p>Applicants this Week</p>
					</div>
					<div class="icon">
						<i class="ion ion-ios-calendar-outline"></i>
					</div>
					<a href="{{ url('/admin/applicants') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>

			<div class="col-lg-3 col-xs-6">
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>{{ $applicants_month }}</h3>
						<p>Applicants this Month</p>
					</div>
					<div class="icon">
						<i class="ion ion-ios-calendar"></i>
					</div>
					<a href="{{ url('/admin/applicants') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>

			<div class="col-lg-3 col-xs-6">
				<div class="small-box bg-red">
					<div class="inner">
						<h3>{{ $applicants_count }}</h3>
						<p>Total Applicants</p>
					</div>
					<div class="icon">
						<i class="ion ion-ios-people"></i>
					</div>
					<a href="{{ url('/admin/applicants') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class = "col-md-4">
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Recent Applicants</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<table class="table table-responsive table-striped">
							<tr>
								<th>Name</th>
								<th>Phone</th>
							</tr>
							@foreach( $applicants  as $applicant )
								<tr>
									<td><a href="{{ url('/admin/applicants/edit/' . $applicant->applicant_id) }}">{{ $applicant->first_name }} {{ $applicant->middle_name }} {{ $applicant->last_name }}</a></td>
									<td>{{ $applicant->phone_number }}</td>
								</tr>
							@endforeach
						</table>
					</div>
					<div class="box-footer text-center">
						<a href="{{ url('/admin/applicants') }}" class="uppercase text-yellow">View All Applicants</a>
					</div>
				</div>
			</div>
			
			<div class = "col-md-4">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Recent Tests</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<table class="table table-responsive table-striped">
							<tr>
								<th>Test Name</th>
								<th>Department</th>
							</tr>
							@foreach( $tests  as $test )
								<tr>
									<td><a href="{{ url('/admin/tests/edit/' . $test->test_id) }}">{{ $test->test_name }}</a></td>
									<td>{{ $test->department_name }}</td>
								</tr>
							@endforeach
						</table>
					</div>
					<div class="box-footer text-center">
						<a href="{{ url('/admin/tests') }}" class="uppercase text-green">View All Tests</a>
					</div>
				</div>
			</div>
			
			<div class = "col-md-4">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Departments</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<table class="table table-responsive table-striped">
							<tr>
								<th>Department Name</th>
								<th>Department ID</th>
							</tr>
							@foreach( $departments as $department )
								<tr>
									<td>{{ $department->department_name }}</td>
									<td>{{ $department->department_id }}</td>
								</tr>
							@endforeach
						</table>
					</div>
					<div class="box-footer text-center">
						<!-- <a href="{{ url('/admin/posts') }}" class="uppercase text-red">View All Posts</a>-->
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

@endsection
