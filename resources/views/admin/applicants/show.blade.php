@extends('admin.layouts.app')


@section('content')

<section class="content-header">
	<h1>Applicant Profile</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{ url('/admin/tests') }}">Applicant</a></li>
		<li class="active">View Profile</li>
	</ol>
</section>
    
<section class="content">
	<div class="row">
		<div class="col-md-4">
			<div class="box box-success">
				<div class="box-body box-profile pl-t">
					<img class="profile-user-img img-responsive img-circle" src="{{ url('/public/theme/admin/img/user-placeholder.jpg') }}" alt="User profile picture">

					<h3 class="profile-username text-center">{{ $applicant->first_name }} {{ $applicant->middle_name }} {{ $applicant->last_name }}</h3>

					<p class="text-muted text-center ml-b"><!-- Software Engineer --></p>

					<ul class="list-group list-group-unbordered">
						<!-- 
						<li class="list-group-item">
							<b>Year(s) Experience</b> <a class="pull-right">1</a>
						</li>
						-->
						<li class="list-group-item">
							<b>Chosen Department:</b> <a class="pull-right">{{ $department->department_name }}</a>
						</li>
					</ul>

					<a href="{{ url('/admin/applicants/edit/' . $applicant->applicant_id) }}" class="btn btn-success btn-block"><b>View Exam Results</b></a>
				</div>
			</div>
		</div>
		
		<div class="col-md-8">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					
					<li class="active"><a href="#basic" data-toggle="tab" aria-expanded="false">Basic Information</a></li>
					<!--
					<li class=""><a href="#activity" data-toggle="tab" aria-expanded="false">Biodata</a></li>
					<li><a href="#timeline" data-toggle="tab" aria-expanded="true">Experiences</a></li>
					-->
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="basic">
						<table class="table table-striped">
							<tr>
								<th>First Name</th>
								<td>{{ $applicant->first_name }}</td>
							</tr>
							<tr>
								<th>Middle Name</th>
								<td>{{ $applicant->middle_name }}</td>
							</tr>
							<tr>
								<th>Last Name</th>
								<td>{{ $applicant->last_name }}</td>
							</tr>
							<tr>
								<th>Email</th>
								<td>{{ $applicant->email }}</td>
							</tr>
							<tr>
								<th>Phone Number</th>
								<td>{{ $applicant->phone_number }}</td>
							</tr>
						</table>
					</div>
					<div class="tab-pane" id="activity">
						
					</div>
					<div class="tab-pane" id="timeline">

					</div>
				</div>
			</div>
		</div>
	</div>
</section>


@endsection