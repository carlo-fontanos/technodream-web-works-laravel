@extends('admin.layouts.app')


@section('content')

<section class="content-header">
	<h1>Applicants <small>Control panel</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Applicants</li>
		<li class="active">All</li>
	</ol>
</section>

<section class="content applicants-index-pagination">
	<div class="box box-success">

		<div class="box-body">
			<form class = "post-list">
				<input type = "hidden" value = "" />
			</form>

			<div class="m-tb clearfix">
				<div class="navbar-form navbar-left p-0 m-0">
					<div class="form-group m-r">
						<label>Per Page: </label>
						<select class="form-control post_max">
							<option value="10">10</option>
							<option value="25">25</option>
							<option value="50">50</option>
						</select>
					</div>
					<div class="form-group m-r">
						<label>Display: </label>
						<select class="form-control post_display">
							<option value="all">All</option>
							<option value="today">Today</option>
							<option value="week">This Week</option>
							<option value="month">This Month</option>
						</select>
					</div>
					{{-- <div class="form-group m-r">
						<label>Working Time: </label>
						<select class="form-control post_working_time">
							<option value="">All</option>
							<option value="night-time">Night Time</option>
							<option value="day-time">Day Time</option>
						</select>
					</div> --}}
					<div class="form-group m-r">
						<label>Department: </label>
						<select class="form-control post_department">
							<option value="0">Select Department</option>
							@foreach( get_departments() as $department)
								<option value="{{ $department->department_id }}">{{ $department->department_name }}</option>
							@endforeach
						</select>
					</div>
					<label>
						Search Keyword:
						<input type="text" placeholder="Enter a keyword" class="form-control post_search_text">
					</label>
					<input type="submit" value="Filter" class="btn btn-success post_search_submit">
				</div>
			</div>

			<br class="clear" />

			<div class="wave-box-wrapper">
				<div class="wave-box"></div>
				<table class="table table-striped table-post-list no-margin">
					<thead>
						<tr>
							<th id="last_name"><a href="#">Last Name</a></th>
							<th id="first_name"><a href="#">First Name</a></th>
							<th id="last_name"><a href="#">Middle Name</a></th>
							<th id="email"><a href="#">Email</a></th>
							<th id="phone_number"><a href="#">Phone</a></th>
							<th id="applicant_test_points"><a href="#">Score</a></th>
							<th id="applicant_test_percentage"><a href="#">Score %</a></th>
							<th id="test_name"><a href="#">Test</a></th>
							<th id="working_time"><a href="#">Working Time</a></th>
							<th id="department_name"><a href="#">Department</a></th>
							<th id="created_at" class="active DESC"><a href="#">Date Applied</a></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody class="pagination-container"></tbody>
				</table>

				<div class="pagination-nav"></div>
			</div>
		</div>
	</div>
</section>


@endsection