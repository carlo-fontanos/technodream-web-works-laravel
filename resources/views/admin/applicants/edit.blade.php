@extends('admin.layouts.app')


@section('content')

<section class="content-header">
	<h1>Applicant Exam</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{ url('/admin/tests') }}">Applicants</a></li>
		<li class="active">View Exam</li>
	</ol>
</section>
    
<section class="content edit-applicant-exam">
	<div class="box box-success">
		
		<div class="box-body">
			<div class="box-header with-border p-0">
				
			</div>
				  
		   <div class = "wave-box-wrapper clearfix pl-tb">
				<div class = "wave-box"></div>
				<table class="table table-bordered">
					<tr>
						<th width="70%">Question #</th>
						<th>Section</th>
						<th class="text-right">Points</th>
					</tr>
					
					<?php $total_points = 0; ?>
					<?php $total_score = 0; ?>
					<?php $applicant_points = 0; ?>
					<?php $applicant_score = 0; ?>
					
					@foreach($result as $question)
						<tr width="70%" class="">
							<td>
								<p class="f-b">{{ $total_score + 1 }}.)</p>
								
								{!! $question->question_content !!}
								
								@if( $question->question_type == 'multiple' )
									<div class="ml-l">
										<?php $alphabet = range('A', 'Z'); ?>
										<?php $choice_alphabet = array(); ?>
										@foreach(get_choices( $question->question_id ) as $choice)
											<div>
												<strong>{{ $alphabet[$choice->choice_order - 1] }}.)</strong> 
												<?php echo $choice->choice_id == $question->correct ?  $choice->choice_content . ' &nbsp; <i class="fa fa-star" aria-hidden="true"></i>': $choice->choice_content; ?>
												<?php $choice_alphabet[$choice->choice_id]= $alphabet[$choice->choice_order - 1]; ?>
											</div>
										@endforeach
									</div>
								@elseif( $question->question_type == 'input' )
									<p class="m-t ml-l ms-b">
										<strong>Correct Answer:</strong> &nbsp;  {{ $question->correct }}
									</p>
								@endif
								
								<?php $alphabet = range('A', 'Z'); ?>
								<p class="m-t ml-l ms-b">
									<strong>Applicant Answer:</strong> &nbsp; 
									
									@if( $question->question_type == 'multiple')
										{{ $choice_alphabet[$question->applicant_choice] }}
									@elseif( $question->question_type == 'input')
										{{ $question->applicant_input }}
									@elseif( $question->question_type == 'essay')
										<p class="ml-l">{!! nl2br(e($question->applicant_input)) !!}</p>
									@endif
								</p>
							</td>
							<td>{{ $question->section_name }}</td>
							<td align="right">
								@if($question->correct)
									{{ $question->points }}
								@elseif($question->question_type == 'essay')
									<input type="number" class="form-control w-a applicant-input-score" min="0" max="{{ $question->question_points }}" id="{{ $question->question_id }}" applicant="{{ $question->applicant_id }}" test="{{ $question->test_id }}" value="{{ $question->points }}" />
								@endif
							</td>
						</tr>
						
						<?php $total_points = $total_points + $question->question_points; ?>
						<?php $total_score = $total_score + 1; ?>
						<?php $applicant_points = $applicant_points + $question->points; ?>
						<?php $applicant_score = $question->points > 0 ? $applicant_score + 1: $applicant_score; ?>
	
					@endforeach
					
					<tbody class="bg-warning">
						<tr>
							<td></td>
							<td class="f-b">Points: </td>
							<td class="f-b" align="right">
								@if($applicant_points && $total_points)
									<span class="total-points">{{ $applicant_points}}</span> / {{ $total_points }}
								@else
									0 / 0
								@endif
							</td>
						</tr>
						<tr>
							<td></td>
							<td class="f-b">Percentage: </td>
							<td class="f-b" align="right"><span class="total-percentage">
								@if($applicant_points && $total_points)
									{{ number_format(($applicant_points / $total_points) * 100, 2) }}</span> / 100
								@else
									0 / 0
								@endif
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

@endsection