@extends('admin.layouts.app')


@section('content')

<section class="content-header">
	<h1>Edit Test <small>- Dynamic Test Editor</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{ url('/admin/tests') }}">Tests</a></li>
		<li class="active">Edit</li>
	</ol>
</section>
    
<section class="content">
	<div class="box box-success">
		
		<div class="box-body">
			<div class="box-header with-border p-0">
				<button class="btn btn-link collapse-all pull-right p-t-0">Toggle Collapse All</button>
			</div>
				  
		   <div class = "wave-box-wrapper clearfix pl-tb">
				<div class = "wave-box"></div>
				
				@if ( isset( $_GET['show-msg'] ) )
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Success!</h4>
						Test successfully created. You are now in the Edit Test page.
					</div>
				@endif
				
				@csrf
				<input type="hidden" name="edit-page" value="{{ $test->test_id }}">
				
				<div class="col-md-6">
					<div class="form-group">
						<label for="department_id">Department:</label>
						<select class="form-control" id="department_id" name="department_id">
							@if (count($departments) > 0)
								@foreach ($departments as $department)
									<option value="{{ $department->department_id }}" {{ $department->department_id == $test->department_id ? "selected" : "" }} >{{ $department->department_name }}</option>
								@endforeach
							@endif
						</select>
					</div>
					<div class="form-group">
						<label for="test_name">Test Name:</label>
						<input type="text" class="form-control" name="test_name" placeholder="Enter test name" value="{{ $test->test_name }}" e-action="update-test-title" e-id="{{ $test->test_id }}" />
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="form-group">
						<label>Test Description:</label>
						<textarea id="ck-editor-area-{{ $test->test_id }}_content" e-action="update-test-content" e-id="{{ $test->test_id }}" class="form-control editor" name="test_content">{{ $test->test_content }}</textarea>
					</div>
					
					<br />
					
					<label>Sections:</label>
					<div class="section-list">	
						
						@if ( isset( $tree->sections ) && ! empty( $tree->sections ) )
							@foreach( $tree->sections as $key_section => $section )
								<div class="panel panel-default section" number="{{ $section->section_order }}" section-id="{{ $section->section_id }}">
									<div class="panel-heading clearfix">
										<a href="" class="fl-r text-danger remove-section"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a>
										<a data-toggle="collapse" href="#collapse-s{{ $section->section_order }}">Section {{ $section->section_order }}</a>
									</div> 
									<div id="collapse-s{{ $section->section_order }}" class="panel-collapse collapse in">
										<div class="panel-body">
											<div class="col-md-6">
												<div class="row">
													<div class="form-group">
														<label for="section-name">Section Name:</label>
														<input type="text" class="form-control" id="section-name" placeholder="Enter section name" name="s{{ $section->section_order }}" value="{{ $section->section_name }}" e-action="update-section-title" e-id="{{ $section->section_id }}" />
													</div>
												</div>
											</div>
											
											<div class="col-md-12">	
												<div class="form-group row">
													<label for="section-desc">Section Description:</label>
													<textarea id="ck-editor-area-s{{ $section->section_order }}_content" e-action="update-section-content" e-id="{{ $section->section_id }}" class="form-control editor" name="s{{ $section->section_order }}_content">{{ $section->section_content }}</textarea>
												</div>
												
												<br />
												
												<div class="row question-container">
													<p class="f-b">Questions:</p>
													<div class="col-md-12">	
														<div class="row question-list">
															@if( isset( $section->questions ) )
																@foreach( $section->questions as $key_question => $question )
																	<?php $arg = 's' . $section->section_order . '_q' . $question->question_order; ?>
																	<div class="panel-group ms-b question" number="{{ $question->question_order }}" id="{{ $arg }}" question-id="{{ $question->question_id }}">
																		<div class="panel panel-success">
																			<div class="panel-heading clearfix">
																				<a href="" class="fl-r text-danger remove-question"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a>
																				<a data-toggle="collapse" href="#collapse-{{ $arg }}">Question {{ $question->question_order }}</a>
																			</div>
																			<div id="collapse-{{ $arg }}" class="panel-collapse collapse in">
																				<div class="panel-body">
																					@if( $question->question_type == 'multiple' )
																						<input type="hidden" name="{{ $arg }}_type" value="multiple" />
																						
																						<div class="form-group">
																							<label>Question:</label>
																							<textarea id="ck-editor-area-{{ $arg }}" e-action="update-question-content" e-id="{{ $question->question_id }}" class="form-control editor" name="{{ $arg }}">{{ $question->question_content }}</textarea>
																						</div>
																						<div class="form-group">
																							<label>Choices:</label>
																							<div class="pl-l">
																								<div class="choices-list">
																									@if( isset( $question->choices ) )
																										@foreach( $question->choices as $key_choice => $choice )
																											<?php $alphabet = range('A', 'Z'); ?>
																											<?php $is_correct = isset( $question->answers ) && in_array( $choice->choice_id, (array) $question->answers ) ? true : false; ?>
																											
																											<div class="input-group ms-b choice" id="{{ $choice->choice_id }}">
																												<span class="input-group-addon f-b letter" number="{{ $choice->choice_order }}">{{ $alphabet[$choice->choice_order - 1] }}</span>
																												<input type="text" class="form-control" name="{{ $arg }}_c{{ $choice->choice_order }}" value="{{ $choice->choice_content }}">
																												<span class="input-group-addon f-b">
																													<input type="checkbox" name="{{ $arg }}_correct_c{{ $choice->choice_order }}" <?php echo $is_correct ? "checked" : ""; ?> class="set-answer">
																												</span>
																												<span class="input-group-addon f-b">
																													<a href="#" class="remove-choice" correct="<?php echo $is_correct ? 1 : 0; ?>"><i class="fa fa-trash-o text-danger"></i></a>
																												</span>
																												
																											</div>
																										@endforeach
																									@endif
																									
																								</div>
																								
																								<a href="#" class="choices-create"><i class="fa fa-plus"></i> Add New Choice</a>
																							</div>
																						</div>
																						<div class="form-group">
																							<label>Points:</label>
																							<input type="text" name="{{ $arg }}_points" class="form-control" value="{{ $question->question_points }}" e-action="update-question-points" e-id="{{ $question->question_id }}" />
																						</div>
																							
																					@elseif( $question->question_type == 'input' )
																						<input type="hidden" name="{{ $arg }}_type" value="input" />
																						
																						<div class="form-group">
																							<label>Question:</label>
																							<textarea id="ck-editor-area-{{ $arg }}" class="form-control editor" name="{{ $arg }}" e-action="update-question-content" e-id="{{ $question->question_id }}">{{ $question->question_content }}</textarea>
																						</div>
																						<div class="form-group">
																							<label>Answer:</label>
																							<input type="text" name="{{ $arg }}_answer" class="form-control" value="{{ $question->answers ? $question->answers[0] : '' }}" e-action="update-question-answer" e-id="{{ $question->question_id }}" />
																						</div>
																						<div class="form-group">
																							<label>Points:</label>
																							<input type="text"  name="{{ $arg }}_points" class="form-control" value="{{ $question->question_points }}" e-action="update-question-points" e-id="{{ $question->question_id }}" />
																						</div>
																							
																					@elseif( $question->question_type == 'essay' )
																						<input type="hidden" name="{{ $arg }}_type" value="essay" />
																						
																						<div class="form-group">
																							<label>Question:</label>
																							<textarea id="ck-editor-area-{{ $arg }}" class="form-control editor" name="{{ $arg }}" e-action="update-question-content" e-id="{{ $question->question_id }}">{{ $question->question_content }}</textarea>
																						</div>
																						<div class="form-group">
																							<label>Points:</label>
																							<input type="text" name="{{ $arg }}_points" class="form-control" value="{{ $question->question_points }}" e-action="update-question-points" e-id="{{ $question->question_id }}" />
																						</div>
																					@endif
																				</div>
																			</div>
																		</div>
																	</div>
																@endforeach
															@endif
															
														</div>
													</div>
													
													<div class="col-md-6">
														<div class="row">
															<div class="form-group ms-t">
																<select class="form-control w-a d-ib question-type">
																	<option value="multiple">Multiple Choices</option>
																	<option value="input">Word(s) input</option>
																	<option value="essay">Essay - long text</option>
																</select>
																<button class="btn btn-link d-ib question-create"><i class="fa fa-plus"></i> Add Question</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						@endif
					</div>
					
					<a href="#" class="section-create"><i class="fa fa-plus"></i> Add New Section</a>
					
					
				</div>
			</div>
		</div>
	</div>
</section>

@endsection