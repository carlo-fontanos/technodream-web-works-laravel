@extends('admin.layouts.app')


@section('content')

<section class="content-header">
	<h1>Create Test <small>- Dynamic Test Maker</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{ url('/admin/tests') }}">Tests</a></li>
		<li class="active">Create</li>
	</ol>
</section>
    
<section class="content">
        <div class="box box-success">
            
            <div class="box-body">
               <div class = "wave-box-wrapper clearfix pl-tb">
					<div class = "wave-box"></div>
					
					<div class="alert alert-danger alert-dismissible response-container d-n">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-ban"></i> Error</h4>
						<ul class = "response-message pl-l"></ul>
					</div>
					
					<form action="{{ url('admin/tests/store-all') }}" method="POST" class = "create-test">
						@csrf
						
						<div class="col-md-6">
							<div class="form-group">
								<label for="department_id">Department:</label>
								<select class="form-control" id="department_id" name="department_id">
									<option value="1">Programming</option>
									<option value="2">SEO</option>
									<option value="5">Sales</option>
									<option value="4">Call Center</option>
									<option value="3">Human Resource</option>
									<option value="6">Designers</option>
								</select>
							</div>
							<div class="form-group">
								<label for="test_name">Test Name:</label>
								<input type="text" class="form-control" name="test_name" placeholder="Enter test name" />
							</div>
						</div>
						
						<div class="col-md-12">	
							<div class="form-group">
								<label>Test Description:</label>
								<textarea id="ck-editor-area-test_content" class="form-control editor" name="test_content"></textarea>
							</div>
							
							<br />
							
							<label>Sections:</label>
							<div class="section-list"></div>
							
							<a href="#" class="section-create"><i class="fa fa-plus"></i> Add New Section</a>
						</div>
						
						
						<div class="col-md-12">	
							<hr />
							
							<input type="submit" class="btn btn-success" value="Create Test" />
						</div>
					</form>
				</div>
            </div>
        </div>
</section>

@endsection