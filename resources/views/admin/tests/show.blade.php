@extends('admin.layouts.app')


@section('content')

<section class="content-header">
	<h1>Tests <small>Control panel</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{ url('/admin/tests') }}">Tests</a></li>
		<li class="active">Show</li>
	</ol>
</section>
    
<section class="content">
        <div class="box box-success">
            
            <div class="box-body">
                Show Content here.
            </div>
        </div>
</section>


@endsection