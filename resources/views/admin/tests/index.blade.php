@extends('admin.layouts.app')


@section('content')

<section class="content-header">
	<h1>Tests <small>Control panel</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Tests</li>
		<li class="active">All</li>
	</ol>
</section>
    
<section class="content tests-index-pagination">
	<div class="box box-success">
		
		<div class="box-body">
			<form class="post-list">
				<input type="hidden" value="" />
			</form>
			
			<div class="m-tb clearfix">
				<div class="navbar-form navbar-left p-0 m-0">
					<div class="form-group m-r">
						<label>Per Page: </label>
						<select class="form-control post_max">
							<option value="10">10</option>
							<option value="25">25</option>
							<option value="50">50</option>
						</select>
					</div>
					<div class="form-group m-r">
						<label>Department: </label>
						<select class="form-control post_department">
							<option value="0">Select Department</option>
							@foreach( get_departments() as $department )
								<option value="{{ $department->department_id }}">{{ $department->department_name }}</option>
							@endforeach
						</select>
					</div>
					<label>
						Search Keyword:
						<input type="text" placeholder="Enter a keyword" class="form-control post_search_text">
					</label>
					<input type="submit" value="Filter" class="btn btn-success post_search_submit">
				</div>
			</div>
			
			<br class="clear" />
			
			<div class="wave-box-wrapper">
				<div class="wave-box"></div>
				<table class="table table-striped table-post-list no-margin">
					<thead>
						<tr>
							<th id="test_name" class="active"><a href="#">Test Name</a></th>
							<th id="department_name"><a href="#">Department</a></th>
							<th id="created_at"><a href="#">Created At</a></th>
							<th>#</th>
						</tr>
					</thead>
					<tbody class="pagination-container"></tbody>
				</table>
				
				<div class="pagination-nav"></div>
			</div>
		</div>
	</div>
</section>


@endsection